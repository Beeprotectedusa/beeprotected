<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $beeKeepers = DB::select('select * from logins where type = 1');
            foreach ($beeKeepers as $beeKeeper) {
                $hives = DB::select('select * from hives where user_id = ?', [$beeKeeper->id]);
                $events = DB::select('select * from sprayevents limit 100');
                $alerts = [];
                $toggle = false;
                $d = strtotime("+3 day");
                $today = date('Y-m-d', $d);
                $count = [];
                foreach ($hives as $hive) {
                    foreach ($events as $event) {
                        $fields = explode("_", $event->field_id_s);
                        foreach ($fields as $field) {
                            if ($field != "") {
                                $field = DB::select('select * from fields where id = ?', [$field]);
                                if (count($field) > 0) {
                                    if (($this->distance($field[0]->lat, $field[0]->lng, $hive->lat, $hive->lng, "M") <= $hive->dist / 1000) && ($event->date <= $today)) {
                                        $eventBuf = clone $event;
                                        $eventBuf->names = $hive->name;
                                        $eventBuf->hive_id = $hive->id;
                                        $eventBuf->user_id = $beeKeeper->id;
                                        $eventBuf->user_name = $beeKeeper->firstName;
                                        $eventBuf->dist = round($this->distance($field[0]->lat, $field[0]->lng, $hive->lat, $hive->lng, "M"), 2);
                                        $eventBuf->user_email = $beeKeeper->email;
                                        array_push($alerts, $eventBuf);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                foreach ($alerts as $alert) {
                    $buf_alert = DB::select('select * from mailalerts where user_id = ? && hive_id = ? && last >= ?', [$alert->user_id, $alert->hive_id, date('Y-m-d')]);
                    if ($buf_alert == []) {
                        DB::insert('insert into `mailalerts` (id,last,user_id,hive_id)
                        values (NULL,?, ?, ?)', [date('Y-m-d'), $alert->user_id, $alert->hive_id]);
                        $details = [
                            'name' => $alert->user_name,
                            'title' => 'Attention!',
                            'body' => "You are registered on the BeeProtected platform as a Beekeeper.
                            We hereby inform you that after 48 hours " . strval($alert->dist) . " from your apiary " . $alert->names . ", the field will be fertilized with insecticides.
                            We strongly recommend to take care of hives and Bees! Check this link below to Login to the website.",
                            'url' => 'https://beeprotectedusa.com/',
                        ];
                        Mail::to($alert->user_email)->locale('en')->send(new Attention($details));
                    }
                }
            }
        })->everyTenMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
