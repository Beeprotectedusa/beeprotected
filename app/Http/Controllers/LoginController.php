<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Login;
use App\Mail\Attention;
use App\Mail\FirstAlert;
use App\Mail\RestorePassword;
use App\Mail\ConnectingBeekeepersAndFarmers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;

class LoginController extends Controller
{
    public function checkUser()
    {
        if (isset($_COOKIE['id']) and isset($_COOKIE['hash'])) {
            $user = DB::select('select * from logins where id = ?', [$_COOKIE['id']]);
            if ($user != []) {
                $user = $user[0];
                if ($_COOKIE['hash'] != $user->hash) {
                    $this->log_out();
                }
            } else {
                $this->log_out();
            }
        }else{
            return redirect()->route('home');
        }
    }
    public function mailer(){
        $str="info@TexasBeeSupply.com

        info@sweetmountainfarm.com
        
        
        melissa@backyardhive.com
        
        info@advancescience.com
        
        info@thehoneyladies.com 
        
        customercare@naturalapiary.com
        
        
        bec@becsbeehive.com.au
        
        info@ohbees.com
        
        
        butlerbees@gmail.com
        
        
        sales@bemishoneybeefarm.com
        
        wrifton@greatlakesgeneralstore.com
        
        
        
        hello@beebuilt.com
        
        
        KBA@kyanabees.com
        
        
        beekeeper@mannlakeltd.com
        
        
        
        dadant@dadant.com
        
        
        rick@westernbee.com
        
        
        
        info@abfnet.org
        
        
        
        
        
        
        editors@agweb.com
        
        
        agonline@agriculture.com
        
        david.kurns@meredith.com
        
        
        
        agropages@vip.163.com
        
        
        feedback@sciencenews.org
        editors@sciencenews.org
        
        
        AgIndustryNews@Farms.com
        
        
        drew.dargis@us.nature.com
        
        EDITOR@EFARMNEWSAR.COM
        
        
        askusda@usda.gov
        
        customerservice@springernature.com
        
        magazinesubmissions@politico.com
        
        
        social@agdaily.com
        
        
        snapshoteditors@dtn.com
        
        
        
        inytletters@nytimes.com
        letters@nytimes.com 
        editorial@nytimes.com
        
        
        editor@farmnews-iowa.com
        
        
        editor@sciencedaily.com
        
        
        info@apinz.org.nz
        
        
        cmichel@nfudc.org
        
        
        info@americanbeejournal.com
        
        
        info@bees.nyc
        
        
        nrvbeekeepersassoc@gmail.com
        
        
        president@massbee.org
        Vicepresident@massbee.org
        newsletter@massbee.org
        registrar@massbee.org
        
        
        форма для связи
        
        
        info@perfectbee.com
        
        
        info@beeculture.com
        
        
        calgaryBeekeepers@gmail.com
        
        
        dadant@dadant.com
        
        
        beverlybees@gmail.com
        
        
        info@beekeepers.co.za
        
        
        jenifer@wasba.org
        
        
        beekeepeRS.indiana@yahoo.com
        
        
        форма для связи
        
        
        форма для связи
        
        
        customerservice@ogdenpubs.com
        
        
        doee@dc.gov
        ";
        $str = explode("\n",$str);
        $str = array_diff($str, ["        ","","        форма для связи"]);
        $str = array_map('trim', $str);
        $emails = ["true918@list.ru","2chhksu@gmail.com","ivlan29072000@gmail.com"];
        $emails = $emails + $str;
        foreach ($emails as $email){
            $details = [
                'name' => '',
                'title' => 'Connecting beekeepers and farmers!',
                'url' => 'https://beeprotectedusa.com/',
            ];
            Mail::to($email)->locale('en')->send(new ConnectingBeekeepersAndFarmers($details));
        }
    }
    public function test()
    {
        $now = date('Y-m-d');
        DB::table('sprayevents')->where('date', '<', $now)->delete();
        $beeKeepers = DB::select('select * from logins where type = 1');
        foreach ($beeKeepers as $beeKeeper) {
            $hives = DB::select('select * from hives where user_id = ?', [$beeKeeper->id]);
            $events = DB::select('select * from sprayevents limit 100');
            $alerts = [];
            $toggle = false;
            $d = strtotime("+3 day");
            $today = date('Y-m-d', $d);
            $count = [];
            foreach ($hives as $hive) {
                foreach ($events as $event) {
                    $fields = explode("_", $event->field_id_s);
                    foreach ($fields as $field) {
                        if ($field != "") {
                            $field = DB::select('select * from fields where id = ?', [$field]);
                            if (count($field) > 0) {
                                if (($this->distance($field[0]->lat, $field[0]->lng, $hive->lat, $hive->lng, "M") <= $hive->dist / 1000) && ($event->date <= $today)) {
                                    $eventBuf = clone $event;
                                    $eventBuf->names = $hive->name;
                                    $eventBuf->hive_id = $hive->id;
                                    $eventBuf->user_id = $beeKeeper->id;
                                    $eventBuf->user_name = $beeKeeper->firstName;
                                    $eventBuf->dist = round($this->distance($field[0]->lat, $field[0]->lng, $hive->lat, $hive->lng, "M"), 2);
                                    $eventBuf->user_email = $beeKeeper->email;
                                    array_push($alerts, $eventBuf);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            foreach ($alerts as $alert) {
                $buf_alert = DB::select('select * from mailalerts where user_id = ? && hive_id = ? && last >= ?', [$alert->user_id, $alert->hive_id, date('Y-m-d')]);
                if ($buf_alert == []) {
                    DB::insert('insert into `mailalerts` (id,last,user_id,hive_id)
                        values (NULL,?, ?, ?)', [date('Y-m-d'), $alert->user_id, $alert->hive_id]);
                    $details = [
                        'name' => $alert->user_name,
                        'title' => 'Attention!',
                        'body' => "You are registered on the BeeProtected platform as a Beekeeper.
                            We hereby inform you that after 48 hours " . strval($alert->dist) . " miles from your apiary " . $alert->names . ", the field will be fertilized with insecticides.
                            We strongly recommend to take care of hives and Bees! Check this link below to Login to the website.",
                        'url' => 'https://beeprotectedusa.com/',
                    ];
                    Mail::to($alert->user_email)->locale('en')->send(new Attention($details));
                }
            }
        }
    }
    public function first()
    {
        if (isset($_COOKIE['id']) and isset($_COOKIE['hash'])) {
            $this->checkUser();
            if ($_COOKIE['type'] == 2) {
                return redirect()->route('farmer');
            } else {
                return redirect()->route('beeKeeper');
            }

        } else {
            return redirect()->route('home');
        }
    }
    public function distance($lat1, $lon1, $lat2, $lon2, $unit)
    {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }

    public function getFields()
    {
        $this->checkUser();
        $fields = DB::select('select * from fields where user_id = ?', [$_COOKIE['id']]);
        if ($fields == []) {
            return redirect()->route('farmer');
        }

        return view('addSprayEvent')->with('fields', $fields);
    }
    public function editField()
    {
        
        $this->checkUser();
        $field = DB::select('select * from fields where id = ? and user_id = ?', [$_GET['id'],$_COOKIE['id']]);
        if ($field == []){
            return redirect()->route('home');
        }
        $field = $field[0];
        return view('editField')->with('field', $field);
    }
    public function editHive()
    {
        $this->checkUser();
        $hive = DB::select('select * from hives where id = ? and user_id = ?', [$_GET['id'], $_COOKIE['id']]);
        if ($hive == []) {
            return redirect()->route('home');
        }
        $hive = $hive[0];
        return view('editHive')->with('hive', $hive);
    }
    public function editSprayEvent()
    {
        $this->checkUser();
        $spray = DB::select('select * from sprayevents where id = ? and user_id = ?', [$_GET['id'], $_COOKIE['id']]);
        if ( $spray  == []) {
            return redirect()->route('home');
        }
        $fields = DB::select('select * from fields where user_id = ?', [$_COOKIE['id']]);
        $spray = $spray[0];
        $spray->list = explode("_", $spray->list);
        $spray->names = explode("_", $spray->names);
        $data = [];
        $data['fields'] = $fields;
        $data['spray'] = $spray;
        return view('editSprayEvent')->with('data', $data);
    }
    public function farmerDownload()
    {
        $this->checkUser();
        $fields = DB::select('select * from fields where user_id = ?', [$_COOKIE['id']]);
        $sprays = DB::select('select * from sprayevents where user_id = ? order by date', [$_COOKIE['id']]);
        foreach ($sprays as $spray) {
            $spray->list = explode("_", $spray->list);
            $spray->names = explode("_", $spray->names);
        }
        $data = [];
        $data['fields'] = $fields;
        $data['sprays'] = $sprays;
        return view('farmer')->with('data', $data);
    }
    public function beeKeeperDownload()
    {
        $this->checkUser();
        $hives = DB::select('select * from hives where user_id = ?', [$_COOKIE['id']]);
        $events = DB::select('select * from sprayevents limit 100');
        $alerts = [];
        $toggle = false;
        $d = strtotime("+3 day");
        $today = date('Y-m-d', $d);
        $count = [];
        foreach ($hives as $hive) {
            foreach ($events as $event) {
                $fields = explode("_", $event->field_id_s);
                foreach ($fields as $field) {
                    if ($field != "") {
                        $field = DB::select('select * from fields where id = ?', [$field]);
                        if (count($field) > 0) {
                            if (($this->distance($field[0]->lat, $field[0]->lng, $hive->lat, $hive->lng, "M") <= $hive->dist / 1000) && ($event->date <= $today)) {
                                $eventBuf = clone $event;
                                $eventBuf->names = $hive->name;
                                array_push($alerts, $eventBuf);
                                break;
                            }
                        }
                    }
                }
            }
        }
        $data = [];
        foreach ($alerts as $spray) {
            if (is_string($spray->list)) {
                $spray->list = explode("_", $spray->list);
            }

        }
        $data['hives'] = $hives;
        $data['alert'] = $alerts;
        return view('beeKeeper')->with('data', $data);
    }
    public function log_con(Request $req)
    {
        if (isset($_COOKIE['id']) and isset($_COOKIE['hash'])) {
            if ($_COOKIE['type'] == 2) {
                return redirect()->route('farmer');
            } else {
                return redirect()->route('beeKeeper');
            }

        } else {
            setcookie("error", "", time() - 3600 * 24 * 30 * 12, "/", null, null, true); // httponly !!!
            return View::make('login');
        }
    }
    public function log_out()
    {
        setcookie("id", "", time() - 3600 * 24 * 30 * 12, "/");
        setcookie("type", "", time() - 3600 * 24 * 30 * 12, "/");
        setcookie("hash", "", time() - 3600 * 24 * 30 * 12, "/"); // httponly !!!
        setcookie("error", "", time() - 3600 * 24 * 30 * 12, "/", null, null, true); // httponly !!!
        return redirect()->route('login_home');
    }
    public function generateCode($length = 6)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHI JKLMNOPRQSTUVWXYZ0123456789";
        $code = "";
        $clen = strlen($chars) - 1;
        while (strlen($code) < $length) {
            $code .= $chars[mt_rand(0, $clen)];
        }
        return $code;
    }
    public function login(Request $req)
    {
        $users = DB::select('select * from logins where email = ?', [$req->input('email')]);
        if ($users == []) {
            setcookie("id", "", time() - 3600 * 24 * 30 * 12, "/");
            setcookie("type", "", time() - 3600 * 24 * 30 * 12, "/");
            setcookie("hash", "", time() - 3600 * 24 * 30 * 12, "/"); // httponly !!!
            setcookie("error", "1", time() + 60 * 60 * 24 * 30, "/", null, null, true); // httponly !!!
            return redirect()->route('login_home');
        }
        $users = $users[0];
        if ($users->password == $req->input('password')) {
            $hash = md5($this->generateCode(10));
            DB::update('update logins set hash = ? where email = ?', [$hash, $req->input('email')]);
            setcookie("id", $users->id, time() + 60 * 60 * 24 * 30, "/");
            setcookie("type", $users->type, time() + 60 * 60 * 24 * 30, "/");
            setcookie("hash", $hash, time() + 60 * 60 * 24 * 30, "/"); // httponly !!!
            setcookie("error", "", time() - 3600 * 24 * 30 * 12, "/", null, null, true); // httponly !!!
            if ($users->type == 2) {
                return redirect()->route('farmer');
            } else {
                return redirect()->route('beeKeeper');
            }

        } else {
            setcookie("id", "", time() - 3600 * 24 * 30 * 12, "/");
            setcookie("type", "", time() - 3600 * 24 * 30 * 12, "/");
            setcookie("hash", "", time() - 3600 * 24 * 30 * 12, "/"); // httponly !!!
            setcookie("error", "1", time() + 60 * 60 * 24 * 30, "/", null, null, true); // httponly !!!
            return redirect()->route('login_home');
        }

    }
    public function email_cheak(Request $req)
    {
        echo "<span class='mesage_error'>Пользователь с таким почтовым адресом уже зарегистрирован</span>";
    }
    public function change()
    {
        $token = $_GET['token'];
        $user = DB::select('select * from logins where token = ?', [$token]);
        if ($user != []) {
            return view('change')->with('token', $token);
        } else {
            return redirect()->route('home');
        }
    }
    public function changePass(Request $req)
    {
        $user = DB::select('select * from logins where token = ?', [$req->input('token')]);
        if ($user != []) {
            $user = $user[0];
            DB::table('logins')
                ->where('id', $user->id)
                ->update(['password' => $req->input('password'),
                    'token' => null,
                ]);
            $details = [
                'name' => $user->firstName,
                'title' => 'Recovering BeeProtected password',
                'body' => 'You have successfully changed your password! Follow this link to login',
                'url' => 'https://beeprotectedusa.com/',
            ];
            \Mail::to($user->email)->locale('en')->send(new \App\Mail\RestorePassword($details));
            return redirect()->route('home');
        } else {
            return redirect()->route('home');
        }
    }
    public function restore(Request $req)
    {
        $user = DB::select('select * from logins where email = ?', [$req->input('email')]);
        if ($user != []) {
            $user = $user[0];
            $token = md5($this->generateCode(10));
            $details = [
                'name' => $user->firstName,
                'title' => 'Recovering BeeProtected password',
                'body' => 'This is to recover your password',
                'url' => 'https://beeprotectedusa.com/change?token=' . $token,
            ];
            DB::update('update logins set token = ? where id = ?', [$token, $user->id]);
            Mail::to($user->email)->send(new RestorePassword($details));
            setcookie("error", "", time() - 3600 * 24 * 30 * 12, "/", null, null, true); // httponly !!!
            return redirect()->route('home');
        } else {
            setcookie("error", "1", time() + 60 * 60 * 24 * 30, "/", null, null, true);
            return redirect()->route('restore');
        }
    }
    public function signUp(Request $req)
    {
        $users = DB::select('select * from logins where email = ?', [$req->input('email')]);
        if ($users == []) {
            $hash = md5($this->generateCode(10));
            $signup = new Login();
            $signup->email = $req->input('email');
            $signup->password = $req->input('password');
            $signup->firstName = $req->input('firstName');
            $signup->secondName = $req->input('secondName');
            $signup->hash = $hash;
            $signup->type = $req->input('type');
            $signup->save();
            $users = DB::select('select * from logins where email = ?', [$req->input('email')]);
            $users = $users[0];
            setcookie("id", $users->id, time() + 60 * 60 * 24 * 30, "/");
            setcookie("type", $users->type, time() + 60 * 60 * 24 * 30, "/");
            setcookie("hash", $hash, time() + 60 * 60 * 24 * 30, "/"); // httponly !!!
            setcookie("error", "", time() - 3600 * 24 * 30 * 12, "/", null, null, true); // httponly !!!
            if ($req->input('type') == 1) {
                $details = [
                    'name' => $req->input('firstName'),
                    'title' => 'You have successfully registered on the BeeProtected platform as a Beekeeper.',
                    'body' => "This is a simple and convenient information platform for communication between beekeepers and farmers, which will help you take care of the bees in advance and protect them from the negative effects of insecticides, significantly reducing the risk of their extinction.
                    You just need to add the location of your hives to the map, and the system will notify you of the upcoming processing without revealing the location of the apiaries in automatic mode.
                    Let's save the Bees together - all you need is to just be in touch!",
                    'url' => 'https://beeprotectedusa.com/',
                ];
                \Mail::to($req->input('email'))->locale('en')->send(new \App\Mail\RestorePassword($details));
            } else {
                $details = [
                    'name' => $req->input('firstName'),
                    'title' => 'You have successfully registered on the BeeProtected platform as a Farmer.',
                    'body' => "This is a simple and convenient information platform for communication between farmers and beekeepers, which will help owners of apiaries adjacent to take care of Bees in advance and protect them from the negative effects of insecticides, and significantly reduce the risk of extinction.
                    You just need to add your fields to the map and set up a schedule for processing insecticides, and the system will automatically notify nearby beekeepers without revealing the location of fields and apiaries of registered users.
                    For unscheduled processing, an emergency alert function is provided.
                    Let's save the Bees together - all you need is to just be in touch!",
                    'url' => 'https://beeprotectedusa.com/',
                ];
                \Mail::to($req->input('email'))->locale('en')->send(new \App\Mail\RestorePassword($details));
            }
            if ($req->input('type') == 2) {
                return redirect()->route('farmer');
            } else {
                return redirect()->route('beeKeeper');
            }

        } else {
            setcookie("id", "", time() - 3600 * 24 * 30 * 12, "/");
            setcookie("type", "", time() - 3600 * 24 * 30 * 12, "/");
            setcookie("hash", "", time() - 3600 * 24 * 30 * 12, "/"); // httponly !!!
            setcookie("error", "2", time() + 60 * 60 * 24 * 30, "/", null, null, true); // httponly !!!
            return redirect()->route('register');
        }
    }
}