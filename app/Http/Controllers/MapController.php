<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MapController extends Controller
{
    public function deleteField()
    {
        $id = $_GET['id'];
        $field = DB::select('select * from fields where id = ? and user_id = ?', [$id ,$_COOKIE['id']]);
        if ($field == []){
            return redirect()->route('home');
        }
        DB::table('fields')->where('id', '=', $id)->delete();
        return redirect()->route('farmer');
    }
    public function deleteHive()
    {
        $id = $_GET['id'];
        $hive = DB::select('select * from hives where id = ? and user_id = ?', [$_GET['id'], $_COOKIE['id']]);
        if ($hive == []) {
            return redirect()->route('home');
        }
        DB::table('hives')->where('id', '=', $id)->delete();
        return redirect()->route('beeKeeper');
    }
    public function deleteSprayEvent()
    {
        $id = $_GET['id'];
        $events = DB::select('select * from sprayevents where id = ? and user_id = ?', [$_GET['id'], $_COOKIE['id']]);
        if ( $events  == []) {
            return redirect()->route('home');
        }
        DB::table('sprayevents')->where('id', '=', $id)->delete();
        return redirect()->route('farmer');
    }
    public function editSprayEvent(Request $req)
    {
        DB::table('sprayevents')
            ->where('id', $req->input('id'))
            ->update(['date' => $req->input('date'),
                'list' => $req->input('list'),
                'field_id_s' => $req->input('fields'),
                'names' => $req->input('names'),
                'time' => $req->input('time')
            ]);
        return redirect()->route('farmer');
    }
    public function addField(Request $req)
    {
        DB::insert('insert into fields (id,name,user_id,address,lat,lng,type,type_id,floweringMargin)
         values (NULL,?, ?, ?, ?, ?, ?, ?, ?)', [$req->input('fieldName'), $_COOKIE['id'],
            "USA", $req->input('lat'), $req->input('lng'), "field", $req->input('CropId'), 1]);
        return redirect()->route('farmer');
    }
    public function addHive(Request $req)
    {
        DB::insert('insert into hives (id,name,user_id,lat,lng,dist)
         values (NULL,?, ?, ?, ?, ?)', [$req->input('hiveName'), $_COOKIE['id'],
            $req->input('lat'), $req->input('lng'), $req->input('Range')]);
        return redirect()->route('beeKeeper');
    }
    public function addSprayEvent(Request $req)
    {
        DB::insert('insert into sprayevents (id,date,user_id,list,field_id_s,names,time)
        values (NULL,?, ?, ?, ?, ?,?)', [$req->input('date'), $_COOKIE['id'], $req->input('list'), $req->input('fields'), $req->input('names'),$req->input('time')]);
        return redirect()->route('farmer');
    }
    public function editField(Request $req)
    {
        DB::table('fields')
            ->where('id', $req->input('id'))
            ->update(['name' => $req->input('fieldName'),
                'address' => "USA",
                'lat' => $req->input('lat'),
                'lng' => $req->input('lng'),
                'type_id' => $req->input('CropId'),
                'floweringMargin' => 1,
            ]);
        return redirect()->route('farmer');
    }
    public function editHive(Request $req)
    {

        DB::table('hives')
            ->where('id', $req->input('id'))
            ->update(['name' => $req->input('fieldName'),
                'lat' => $req->input('lat'),
                'lng' => $req->input('lng'),
                'dist' => $req->input('Range'),
            ]);
        return redirect()->route('beeKeeper');
    }
}
