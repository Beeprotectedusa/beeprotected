@extends('layouts.app')

@section('title-block')
A valuable tool connecting beekeepers with farmers & informing of crop protection activities nearby
@endsection

@section('content')
<script>
function validate_form() {
    "use strict"
    let valid = true;

    if (document.contact_form.email.value == "") {
        document.contact_form.email.className = "form-control is-invalid";
        valid = false;
    } else {
        document.contact_form.email.className = "form-control is-valid";
    }
    if (document.contact_form.password.value == "") {
        document.contact_form.password.className = "form-control is-invalid";
        valid = false;
    } else {
        document.contact_form.password.className = "form-control is-valid";
    }

    return valid;

}
</script>

<div class="container-fluid main-container">
    <div class="container-fluid" style="display:flex; padding-top: 15px; align-items: flex-start;  background-color: #FFF; border: solid 1px #e2e2e2;">
        <div class="row">
        @if (isset($_COOKIE['id']) and isset($_COOKIE['hash']))
            <div class="col-md-7 mt-md-0 mt-3 animate">
            <h3>Welcome!</h3>
                <p>Welcome to BeeProtected – bringing farmers and beekeepers together, and keeping beekeepers notified
                    when a neighbouring farmer is applying insecticides to their crops.</p>
                <h3>Farmers &amp; Spray Contractors</h3>
                <p>Please <a href="/register" title="Register">register as a farmer</a>, enter the
                    details of when and where you're planning to spray an insecticide that may present a risk to bees
                    (for instance on a flowering crop, or where the field has a conservation buffer strip), and a simple
                    notification will be sent to neighbouring beekeepers registered with the system.</p>
                <h3> Beekeepers</h3>
                <p>
                    Please <a href="/register" title="Register">register as a
                        beekeeper,</a> enter the position of your hives on our easy to
                    use mapping system, and you will receive notifications from registered farmers when they are
                    planning to spray their crops at a proximity of your choosing up to 5miles away.
                </p>
            </div>
            <div class="col-md-5 mt-md-0 mt-3 animate">
                <div class="container-fluid" style="width:100%; max-width: 600px;  margin-top: 5%; ">                  
                    <div  width="360" class="fb-page" data-href="https://www.facebook.com/Bee-Protected-632676160707663/" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Bee-Protected-632676160707663/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Bee-Protected-632676160707663/">Bee Protected</a></blockquote></div>
                </div>
            </div>
            @else
            <div class="col-md-7 mt-md-0 mt-3 animate">
                <h3>Welcome!</h3>
                <p>Welcome to BeeProtected – bringing farmers and beekeepers together, and keeping beekeepers notified
                    when a neighbouring farmer is applying insecticides to their crops.</p>
                <h3>Farmers &amp; Spray Contractors</h3>
                <p>Please <a href="/register" title="Register">register as a farmer</a>, enter the
                    details of when and where you're planning to spray an insecticide that may present a risk to bees
                    (for instance on a flowering crop, or where the field has a conservation buffer strip), and a simple
                    notification will be sent to neighbouring beekeepers registered with the system.</p>
                <h3> Beekeepers</h3>
                <p>
                    Please <a href="/register" title="Register">register as a
                        beekeeper,</a> enter the position of your hives on our easy to
                    use mapping system, and you will receive notifications from registered farmers when they are
                    planning to spray their crops at a proximity of your choosing up to 5miles away.
                </p>
                <p class="loginPrivacyNotice">View our <a href="/privacy" title="Privacy">privacy statement.</a></p>
                    <form name="contact_form" action="/signin" method="POST" onsubmit="return validate_form()"
                        novalidate>
                        @csrf
                        <div class="input-group mb-3">
                            <input name="email" type="text" class="form-control"
                                style="border-radius: 25px;  height: 50px;" placeholder="Your email..."
                                aria-label="Имя пользователя" aria-describedby="basic-addon1">
                        </div>
                        <div class="input-group mb-3">
                            <input name="password" type="password" class="form-control"
                                style="border-radius: 25px; height: 50px;" placeholder="Password..."
                                aria-label="Имя получателя" aria-describedby="basic-addon2">
                        </div>
                        @if (isset($_COOKIE['error']))
                        @if (($_COOKIE['error'])=="1")
                        <p class="text-danger " style="padding-left: 10px;" id="danger">incorrect Password or Login</p>
                        @endif
                        @endif
                        <div class="controls">
                            <div class="column">
                                <a href="/restore" class="">Restore password</a>
                            </div>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" onClick='location.href="/register"' class="btn btn-secondary"
                                    style="border-radius: 50px 0 0 50px; width: 100px;">Sign up</button>
                                <button type="submit" style="border-radius: 0 50px 50px 0; width: 100px"
                                    class="btn btn-success">Log In</button>
                            </div>
                        </div>
                        <div class="effect jaques">
                            <div class="buttons">
                                <div class="row">
                                    <a href="http://www.facebook.com/sharer.php?u=https://beeprotectedusa.com/&t=Connecting beekeepers and farmers!"
                                        target="_blank" class="fb" title="On Facebook"><i class="fa fa-facebook"
                                            aria-hidden="true"></i></a>
                                    <a href="http://twitter.com/share?url=https://beeprotectedusa.com/&text=Connecting beekeepers and farmers!"
                                        class="tw" title="On Twitter" target="_blank"><i class="fa fa-twitter"
                                            aria-hidden="true"></i></a>
                                    <a href="http://www.linkedin.com/shareArticle?mini=true&url=https://beeprotectedusa.com/"
                                        target="_blank" class="g-plus" title="On linkedin"><i class="fa fa-linkedin "
                                            aria-hidden="true"></i></a>
                                    <a href="#" class="vimeo" title="Join us on Vimeo"><i class="fa fa-vimeo"
                                            aria-hidden="true"></i></a>
                                    <a href="#" class="pinterest" title="Join us on Pinterest"><i
                                            class="fa fa-pinterest-p" aria-hidden="true"></i></a>
                                    <a href="#" class="insta" title="Join us on Instagram"><i class="fa fa-instagram"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </form>
            </div>
            <div class="col-md-5 mt-md-0 mt-3 animate">
                <div class="container-fluid" style="width:100%; max-width: 600px;  margin-top: 5%; ">                  
                    <div  width="360" class="fb-page" data-href="https://www.facebook.com/Bee-Protected-632676160707663/" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Bee-Protected-632676160707663/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Bee-Protected-632676160707663/">Bee Protected</a></blockquote></div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>

@endsection