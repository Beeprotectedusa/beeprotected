@extends('layouts.app')
@section('title-block')
Terms
@endsection
@section('content')
<div class="row" style="margin-right: 0px;">
    <div class="col-md-2 mt-md-0 mt-3">
        <div class="d-md-none">horizontal</div>
        <div class="d-none d-md-block">Vertical</div>
    </div>
    <div class="col-md-8 mt-md-0 mt-3" style=" margin-right: 15px; padding-top: 15px;">
        <div class="container-fluid">
            <h3>
                To make this site simpler to use, we sometimes place small files on your computer. These are known as
                cookies, almost all websites do this too. Cookies have many uses and most are not damaging to your
                privacy, all of the cookies used on this web site fall into that category. We do not use cookies that
                collect or transfer your personal data to other web sites for advertising or any other purpose.
            </h3>
            <hr align="center" width="98%" size="2" color="#6c757d" />
            <p>
                Cookies are used on this site purely to improve services for you through, for example:
                <ul>
                    <li><span>enabling a service to recognise your device so you don't have to give the same information
                            several times during one task</span></li>
                    <li><span>recognising that you may already have given a username and password so you don't need to
                            do it for every web page requested</span></li>
                    <li><span>measuring how many people are using the site, so it can be made easier to use and there's
                            enough capacity to ensure it is fast enough</span></li>
                </ul>
                The following cookie is used to provide other normal website functions:
                <ul>
                    <li>
                        hash: stores a randomly generated number that helps us to indentify user
                    </li>
                    <li>
                        error: stores system errors  until you close your browser
                    </li>
                    <li>
                        type: beekeeper or farmer
                    </li>
                </ul>
            </p>

        </div>
    </div>
    <div class="col-md-1 mt-md-0 mt-3">
        <div class="d-md-none">horizontal</div>
        <div class="d-none d-md-block">Vertical</div>
    </div>
</div>

@endsection