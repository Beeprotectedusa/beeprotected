@extends('layouts.app')

@section('title-block')
Change
@endsection

@section('content')
<script>
function validate_form() {
    "use strict"
    let valid = true;


    if (document.contact_form.password.value == "") {
        document.contact_form.password.className = "form-control is-invalid";
        valid = false;
    } else {
        document.contact_form.password.className = "form-control is-valid";
    }
    if (document.contact_form.confirm.value == "") {
        document.contact_form.confirm.className = "form-control is-invalid";
        valid = false;
    } else {
        document.contact_form.confirm.className = "form-control is-valid";
    }
    if (document.contact_form.password.value != document.contact_form.confirm.value) {
        document.getElementById('danger').hidden = false;
        valid = false;
    } 

    return valid;

}
</script>
<div class="container-fluid"
    style="display:flex; Justify-content: center; margin-right: 0px; background-color: #f9f9f9; height: 510px;">
    <div class="col-md-8 mt-md-0 mt-3"
        style="padding-top: 15px; padding-bottom: 15px; background-color: #FFF; border: solid 1px #e2e2e2;">
        <div class="container-fluid" style="width:100%; max-width: 600px;  margin-top: 5%; ">

        <h3>Please, enter new password </h3>
            <form name="contact_form" action="/change" method="POST" onsubmit="return validate_form()" novalidate>
                @csrf
                <div class="input-group mb-3">
                    <input name="password" type="password" class="form-control" style="border-radius: 25px;  height: 50px;"
                        placeholder="New password" aria-label="Имя пользователя" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-3">
                    <input name="confirm" type="password" class="form-control"
                        style="border-radius: 25px; height: 50px;" placeholder="Confirm your password" aria-label="Имя получателя"
                        aria-describedby="basic-addon2">
                </div>
                <p class="text-danger " style="padding-left: 10px;" id="danger" hidden="true">   Attention: your "Password" and "Confirm Password" do not match, please, try again</p>
                <div class="controls">
                        <div class="column">
                    
                        </div>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="submit" style=" width: 100px"
                                class="btn btn-success">Submit</button>
                        </div>
                    </div>
               <input name="token" type="text" hidden="true" value={{$token}}>
            </form>
        </div>
    </div>
</div>
@endsection
