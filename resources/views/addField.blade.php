@extends('layouts.app')
@section('title-block')
Add Field
@endsection
@section('content')
<div class="row" style="margin-right: 0px; background-color: #f9f9f9;">
    <div class="col-md-2 mt-md-0 mt-3">

    </div>
    <div class="col-md-8 mt-md-0 mt-3 container-main"
        style="padding-top: 15px; padding-bottom: 60px; background-color: #FFF; border: solid 1px #e2e2e2;">
        <div class="container-fluid">
            <h1>
                Add Field
            </h1>
            <p>
                To add a field, give it a name and then locate it on the map by adding a nearby postcode or location
                into the search box. <strong>Once you have
                    located your field, click the map to add a pin to the correct location.</strong> You can also
                declare whether the field has a flowering margin, and
                the type of crop being grown. <br> When you’re happy with the location, simply click “Submit”.
            </p>
            <script>
            function valuedate() {
                let valid = true;

                if (document.contact_form.fieldName.value == "") {
                    document.contact_form.fieldName.className = "form-control is-invalid";
                    valid = false;
                } else {
                    document.contact_form.fieldName.className = "form-control is-valid";
                }
                if (document.contact_form.lat.value == "") {
                    document.getElementById('alert').hidden = false;
                    document.getElementById('type-alert').hidden = false;
                    valid = false;
                }
                return valid;
            }
            </script>
            <form name="contact_form" method="POST" action="/addField" onsubmit="return valuedate ();" novalidate>
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Name</label>
                        <input type="text" class="form-control" name="fieldName" id="fieldName">
                        <label for="inputAddress">Location</label>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" aria-describedby="basic-addon2" id="pac-input">
                            <div class="input-group-append">
                                <button type="button" class=" btn btn-success" id="searchbutton">Search</button>
                            </div>
                        </div>
                        <label for="inputAddress">Crop</label>
                        <select class="form-control valid" data-val="true"
                            data-val-number="The field CropId must be a number."
                            data-val-required="The CropId field is required." id="CropId" name="CropId"
                            aria-required="true" aria-describedby="CropId-error" aria-invalid="false">
                            <optgroup label="Cereals/Oilseeds">
                                <option value="1">Wheat</option>
                                <option value="2">Barley</option>
                                <option value="3">Oats</option>
                                <option value="4">Rye</option>
                                <option value="5">Triticale</option>
                                <option value="6">Millet</option>
                                <option value="7">Sorghum</option>
                                <option value="8">Oilseed Rape</option>
                                <option value="9">Linseed</option>
                                <option value="10">Peas</option>
                                <option value="11">Beans</option>
                                <option value="12">Potatoes</option>
                                <option value="13">Sugar Beet</option>
                                <option value="14">Maize</option>
                                <option value="15">Other</option>
                            </optgroup>
                            <optgroup label="Fresh Vegetables">
                                <option value="16">Tomatoes</option>
                                <option value="17">Brassicas</option>
                                <option value="18">Carrots</option>
                                <option value="19">Onions</option>
                                <option value="20">Celery</option>
                                <option value="21">Cucumbers</option>
                                <option value="22">Lettuce</option>
                                <option value="23">Turnips</option>
                                <option value="24">Other</option>
                            </optgroup>
                            <optgroup label="Soft Fruit">
                                <option value="25">Strawberries</option>
                                <option value="26">Blackcurrants</option>
                                <option value="27">Redcurrants</option>
                                <option value="28">Gooseberries</option>
                                <option value="29">Blueberries</option>
                                <option value="30">Raspberries</option>
                                <option value="31">Blackberries</option>
                                <option value="32">Vines</option>
                                <option value="33">Other</option>
                            </optgroup>
                            <optgroup label="Top Fruit">
                                <option value="34">Apples</option>
                                <option value="35">Pears</option>
                                <option value="36">Plums</option>
                                <option value="37">Cherries</option>
                                <option value="38">Peaches</option>
                                <option value="39">Other</option>
                            </optgroup>
                            <optgroup label="Other">
                                <option value="40">Hoes</option>
                                <option value="41">Grass</option>
                                <option value="42">Wildflowers</option>
                                <option value="43">Borage</option>
                                <option value="44">Other</option>
                            </optgroup>
                        </select>
                        <div style="margin-top: 15px;">
                            <input data-val="true"
                                data-val-required="The This field contains a flowering margin field is required."
                                id="floweringMargin" name="floweringMargin" type="checkbox" value="true"><label
                                class="optional" for="floweringMargin">This field contains a flowering margin</label>
                        </div>
                        <input type="text" class="form-control" aria-describedby="basic-addon2" name="lat" id="lat" hidden="true">
                        <input type="text" class="form-control" aria-describedby="basic-addon2" name="lng" id="lng" hidden="true">
                        <input type="text" class="form-control" aria-describedby="basic-addon2" name="location" id="location"
                            hidden="true">
                        <div class="alert alert-danger" hidden="true" id="alert" style="margin-top: 25px;">
                            <ul>
                                <li class="text-danger " hidden="true" id="type-alert">
                                    Please, add marker of your field
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <div id="map"></div>
                        <script>
                        function initAutocomplete() {
                            var map = new google.maps.Map(document.getElementById('map'), {
                                center: {
                                    lat: 39.10154048781879,
                                    lng: -94.57899425343108
                                },

                                zoom: 13,
                                mapTypeId: 'roadmap'
                            });
                            var geocoder = new google.maps.Geocoder();
                            var input = document.getElementById('pac-input');
                            var searchBox = new google.maps.places.SearchBox(input);
                            var button = document.getElementById('searchbutton');
                            map.addListener('bounds_changed', function() {
                                searchBox.setBounds(map.getBounds());
                            });
                            map.addListener('click', function(e) {
                                placeMarker(e.latLng, map);
                            });
                            var markers = [];
                            var address;

                            function geocodeLatLng(lat1, lng1) {
                                var latlng = {
                                    lat: lat1,
                                    lng: lng1
                                };
                                geocoder.geocode({
                                    'location': latlng
                                }, function(results, status) {
                                    if (status === 'OK') {
                                        if (results[0]) {
                                            address = results[0].formatted_address;
                                        } else {
                                            window.alert('No results found');
                                        }
                                    } else {
                                        window.alert('Geocoder failed due to: ' + status);
                                    }
                                });
                            }

                            function placeMarker(position, map) {
                                markers.forEach(function(marker) {
                                    marker.setMap(null);
                                });
                                markers = [];
                                markers.push(new google.maps.Marker({
                                    map: map,
                                    position: position
                                }));
                                document.getElementById('lat').value = markers[0].getPosition().lat();
                                document.getElementById('lng').value = markers[0].getPosition().lng();
                                map.panTo(position);
                            }
                            button.onclick = function() {
                                var location = searchBox.getPlaces();
                                if (location.length == 0) {
                                    return;
                                }
                                var bound = new google.maps.LatLngBounds();
                                location.forEach(function(place) {
                                    if (!place.geometry) {
                                        console.log("Returned place contains no geometry");
                                        return;
                                    }
                                    var icon = {
                                        url: place.icon,
                                        size: new google.maps.Size(71, 71),
                                        origin: new google.maps.Point(0, 0),
                                        anchor: new google.maps.Point(17, 34),
                                        scaledSize: new google.maps.Size(25, 25)
                                    };

                                    // Create a marker for each place.

                                    if (place.geometry.viewport) {
                                        // Only geocodes have viewport.
                                        bound.union(place.geometry.viewport);
                                    } else {
                                        bound.extend(place.geometry.location);
                                    }
                                });
                                map.fitBounds(bound);
                            }
                            searchBox.addListener('places_changed', function() {
                                var places = searchBox.getPlaces();

                                if (places.length == 0) {
                                    return;
                                }
                                var bounds = new google.maps.LatLngBounds();
                                places.forEach(function(place) {
                                    if (!place.geometry) {
                                        console.log("Returned place contains no geometry");
                                        return;
                                    }
                                    var icon = {
                                        url: place.icon,
                                        size: new google.maps.Size(71, 71),
                                        origin: new google.maps.Point(0, 0),
                                        anchor: new google.maps.Point(17, 34),
                                        scaledSize: new google.maps.Size(25, 25)
                                    };

                                    // Create a marker for each place.

                                    if (place.geometry.viewport) {
                                        // Only geocodes have viewport.
                                        bounds.union(place.geometry.viewport);
                                    } else {
                                        bounds.extend(place.geometry.location);
                                    }
                                });
                                map.fitBounds(bounds);
                            });
                        }
                        </script>
                    </div>
                    <button type="submit" class="btn btn-success"
                        style="border-radius: 50px; width: 150px">Submit</button>
            </form>
        </div>
    </div>
    <div class="col-md-2 mt-md-0 mt-3">

    </div>
</div>
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUzxzIDbmGj57fGikAKtxkh7YU5s01Vb0&libraries=places&callback=initAutocomplete"
    async defer></script>
@endsection