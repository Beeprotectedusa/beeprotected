@extends('layouts.app')

@section('title-block')
Login
@endsection

@section('content')
<script>
function validate_form() {
    "use strict"
    let valid = true;

    if (document.contact_form.email.value == "") {
        document.contact_form.email.className = "form-control is-invalid";
        valid = false;
    } else {
        document.contact_form.email.className = "form-control is-valid";
    }
    if (document.contact_form.password.value == "") {
        document.contact_form.password.className = "form-control is-invalid";
        valid = false;
    } else {
        document.contact_form.password.className = "form-control is-valid";
    }

    return valid;

}
</script>
<div class="container-fluid"
    style="display:flex; Justify-content: center; margin-right: 0px; background-color: #f9f9f9; height: 510px;">
    <div class="col-md-8 mt-md-0 mt-3"
        style="padding-top: 15px; padding-bottom: 15px; background-color: #FFF; border: solid 1px #e2e2e2;">
        <div class="container-fluid" style="width:100%; max-width: 600px;  margin-top: 5%; ">

            <p class="loginPrivacyNotice">View our <a href="/privacy" title="Privacy">privacy statement.</a></p>
            <form name="contact_form" action="/signin" method="POST" onsubmit="return validate_form()" novalidate>
                @csrf
                <div class="input-group mb-3">
                    <input name="email" type="text" class="form-control" style="border-radius: 25px;  height: 50px;"
                        placeholder="Your email..." aria-label="Имя пользователя" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-3">
                    <input name="password" type="password" class="form-control"
                        style="border-radius: 25px; height: 50px;" placeholder="Password..." aria-label="Имя получателя"
                        aria-describedby="basic-addon2">
                </div>
                @if (isset($_COOKIE['error']))
                @if (($_COOKIE['error'])=="1")
                <p class="text-danger " style="padding-left: 10px;" id="danger">incorrect Password or Login</p>
                @endif
                @endif

                <div class="controls">
                    <div class="column">
                        <a href="/restore" class="">Restore password</a>
                    </div>
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" onClick='location.href="/register"' class="btn btn-secondary"
                            style="border-radius: 50px 0 0 50px; width: 100px;">Sign up</button>
                        <button type="submit" style="border-radius: 0 50px 50px 0; width: 100px"
                            class="btn btn-success">Log In</button>
                    </div>
                </div>
                <div class="effect jaques animate">
                    <div class="buttons">
                        <div class="row">
                            <a href="http://www.facebook.com/sharer.php?u=https://beeprotectedusa.com/&t=Connecting beekeepers and farmers!"
                                target="_blank" class="fb" title="On Facebook"><i class="fa fa-facebook"
                                    aria-hidden="true"></i></a>
                            <a href="http://twitter.com/share?url=https://beeprotectedusa.com/&text=Connecting beekeepers and farmers!"
                                class="tw" title="On Twitter" target="_blank"><i class="fa fa-twitter"
                                    aria-hidden="true"></i></a>
                            <a href="http://www.linkedin.com/shareArticle?mini=true&url=https://beeprotectedusa.com/"
                                target="_blank" class="g-plus" title="On linkedin"><i class="fa fa-linkedin "
                                    aria-hidden="true"></i></a>
                            <a href="#" class="vimeo" title="Join us on Vimeo"><i class="fa fa-vimeo"
                                    aria-hidden="true"></i></a>
                            <a href="#" class="pinterest" title="Join us on Pinterest"><i class="fa fa-pinterest-p"
                                    aria-hidden="true"></i></a>
                            <a href="#" class="insta" title="Join us on Instagram"><i class="fa fa-instagram"
                                    aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection