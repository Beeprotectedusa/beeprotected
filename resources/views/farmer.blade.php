@extends('layouts.app')
@section('title-block')
Farmer
@endsection
@section('content')
<div class="row " style="margin-right: 0px;">
    <div class="col-md-2 mt-md-0 mt-3 side">

    </div>

    <div class="col-md-8 mt-md-0 mt-3 container-main-text" style="">
        <div class="row" style="align-items: center; margin-left: 0px; dysplay: flex;  justify-content:space-between;">
            <div class="column" style=" padding-left: 20px; dysplay: flex;">
                <h1 style="font-size: 1.6em; padding-top: 15px; ">
                    Your Fields
                </h1>
                <p> This is a list of your fields.</p>
            </div>
            <button type="button" class=" btn btn-success add-btn" onClick='location.href="/add"'>Add Field</button>
        </div>
        <div>
            <div class="cont">
                @forelse ($data['fields'] as $field) <div class="col-xs-12 col-sm-6">
                    <div clss="card" style=" border: solid 1px #ccc; margin-bottom: 15px;">
                        <h5 class=" card-header">{{$field->name}}</h5>
                        <div class="card-body" style="overflow:hidden;">
                            <div x-data="{ show: false }">
                                <a class="alert-link unselectable" x-on:click="show= ! show"
                                    x-text="show ? 'Location (Hide)': 'Location (Show)' "> </a>
                                <p class="card-text" x-show.transition.origin.top.right="show">Coords:
                                    {{$field->lat}},{{$field->lng}}</p>
                                <hr />
                            </div>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" @click="deleteTodo(todo.id)" class="btn btn-danger"
                                    style="border-radius: 50px 0 0 50px; width: 100px;"
                                    onClick='location.href="/deleteField?id={{$field->id}}"'>Delete</button>
                                <button type="button" style="border-radius: 0 50px 50px 0; width: 100px"
                                    class="btn btn-success"
                                    onClick='location.href="/editField?id={{$field->id}}"'>Edit</button>
                            </div>
                        </div>
                    </div>
                </div>
                @empty
                <div class="text-muted" style="width: 100%; text-align: center">
                    <h5>No fields</h5>
                </div>
                @endforelse
            </div>
        </div>
        <div class="row" style="align-items: center; dysplay: flex; margin-left: 0px; justify-content:space-between;">
            <div class="column" style=" padding-left: 20px; dysplay: flex;">
                <h1 style="font-size: 1.6em; padding-top: 15px; ">
                    Your Spray Events
                </h1>
                <p> This is a list of your Spray Events.</p>
            </div>
            @if ($data['fields'] != [])
            <button type="button" class=" btn btn-success add-btn" onClick='location.href="/addSprayEvent"'>Add Spray
                Event</button>
            @else
            <button type="button" class=" btn btn-secondary add-btn"
                onClick="document.getElementById('msg_error').style.display='block';document.getElementById('msg_error').className += 'fadeIn'; return false;">Add
                Spray Event</button>
                <div id="msg_error" style="display: none;">
                <span class="msg_close"
                    onclick="document.getElementById('msg_error').style.display='none'; document.getElementById('msg_error').className += 'fadeOut'; return false;">X</span>
                <h4 class="unselectable" style="margin-bottom: 15px;">You haven't  added any fields to your account. </h4>
            </div>
            @endif
        </div>
        <div style="padding-bottom: 30px;">
            <div class="cont">
                @forelse ($data['sprays'] as $spray)
                <div class="col-xs-12 col-sm-6">
                    <div clss="card" style=" border: solid 1px #ccc; margin-bottom: 15px;">
                    <div class="card-header">
                            <h5 >Date: {{$spray->date}}</h6>
                            <h6 >Time: {{$spray->time}}</h6>
                        </div>
                        <div class="card-body" style="overflow:hidden;">
                            <div x-data="{ show: false }">
                                <a class="alert-link unselectable" x-on:click="show= ! show"
                                    x-text="show ? 'Insectides (Hide)': 'Insectides (Show)' "> </a>
                                @foreach($spray->list as $list)
                                <h6 class="card-subtitle mb-2 text-muted unselectable"
                                    x-show.transition.origin.top.right="show">
                                    {{$list}}</h6>
                                @endforeach
                                <hr />
                            </div>
                            <div x-data="{ show: false }">
                                <a class="alert-link unselectable" x-on:click="show= ! show"
                                    x-text="show ? 'Fields (Hide)': 'Fields (Show)' "> </a>
                                @foreach($spray->names as $name)
                                <h6 class="card-subtitle mb-2 text-muted unselectable"
                                    x-show.transition.origin.top.right="show">
                                    {{$name}}</h6>
                                @endforeach
                                <hr />
                            </div>
                            <div class="btn-group"  role="group" aria-label="Basic example">
                                <button type="button" @click="deleteEvent(spray.id)" class="btn btn-danger"
                                    style="border-radius: 50px 0 0 50px; width: 100px;"
                                    onClick='location.href="/deleteSprayEvent?id={{$spray->id}}"'>Delete</button>
                                <button type="button" style="border-radius: 0 50px 50px 0; width: 100px"
                                    class="btn btn-success"
                                    onClick='location.href="/editSprayEvent?id={{$spray->id}}"'>Edit</button>
                            </div>
                        </div>
                    </div>
                </div>
                @empty
                <div class="text-muted" style="width: 100%; text-align: center">
                    <h5>No Spray Events</h5>
                </div>
                @endforelse
            </div>
        </div>
        
    </div>
    <div class="col-md-2 mt-md-0 mt-3 side">

    </div>
    @endsection