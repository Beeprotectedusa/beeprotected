@component('mail::message')
# {{ $details['title'] }}

<h4 stle="text-color:  #d9534f">Attention, {{ $details['name'] }}</h4>
{{ $details['body'] }}

<a href="{{ $details['url'] }}">{{ $details['url'] }}</a>

@component('mail::button', ['url' => $details['url']])
Click here!
@endcomponent

Taking care of You and Bees,<br>
Administration of BeeProtected 
@endcomponent
