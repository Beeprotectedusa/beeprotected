@component('mail::message')

<h4 stle="text-color:  #d9534f">To whom it may concern,</h4>
<p>Our team launched the BeeProtected service, which aims to protect the environment, reduce the risk of poisoning bees and reduce their numbers. This is a platform through which Farmers will be able to notify Beekeepers about the beginning of cultivation of crops.</p>
</br>
<h4 stle="text-color:  #d9534f">The platform is absolutely free!</h4>
</br>
<p>When connecting to the system, Beekeepers need to indicate the exact location of their apiaries, and Farmers - the fields. Each registered farmer will have his own personal account, where he can indicate the exact time and method of processing, as well as the drugs used.</p>
<p>Beekeepers whose apiaries are within a radius of 5 miles from the cultivated area will receive an e-mail alert and can take timely measures to protect insects. </p>
<p>Late informing of Beekeepers, lack of communication between them and Farmers can “unintentionally” lead to mass death of Bees.</p>
<p>We are constantly improving the service, focusing on the opinions of Farmers and Beekeepers, and new regulatory requirements.</p>
<p>Together we can stop the mass death of Bees!</p>
</br>
<p>Among the main reasons for the death of Bees, experts identified the incorrect use of pesticides (including those containing organic compounds neonicotinoids), as well as the low level of professionalism of farmers. Often, farmers are unaware of the correct use of chemicals and work the fields in violation of the rules. And more than 70% of the world's crops, on which the global food system depends, are pollinated by Bees. According to experts from the UN Food and Agriculture Organization, Bees and other pollinators bring benefits to the world economy in the amount of more than $ 500 trillion. To draw attention to the problem of the disappearance of Bees, World Bee Day is celebrated on 20 May.</p>
</br>
<p>We kindly ask you to bring the letter to all interested parties and register on the portal www.BeeProtectedUsa.com, if you are a Farmer or Beekeeper, and place the link on your portal.</p>
</br>
<a href="{{ $details['url'] }}">{{ $details['url'] }}</a>
@component('mail::button', ['url' => $details['url']])
Click here to go to website!
@endcomponent

Respectfully,<br>
BeeProtected site Administration
@endcomponent
