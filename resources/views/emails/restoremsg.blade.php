<!DOCTYPE html>
<html>
<head>
    <title>beeprotectedusa.com</title>
</head>
<body>
    <h1>{{ $details['title'] }}</h1>
    <p>{{ $details['body'] }}</p>
    <a href="{{ $details['url'] }}">{{ $details['url'] }}</a>
    <p>Thank you</p>
</body>
</html>