@extends('layouts.app')
@section('title-block')
Register
@endsection
@section('content')


<div class="row" style="margin-right: 0px; background-color: #f9f9f9;">
    <div class="col-md-2 mt-md-0 mt-3">

    </div>
    <div class="col-md-8 mt-md-0 mt-3"
        style="padding-top: 15px; padding-bottom: 15px; background-color: #FFF; border: solid 1px #e2e2e2;">
        <div class="container-fluid">
            <h1>
                Register
            </h1>
            <script>
            function valuedate() {
                let valid = true;

                if (document.contact_form.email.value == "") {
                    document.contact_form.email.className = "form-control is-invalid";
                    valid = false;
                } else {
                    document.contact_form.email.className = "form-control is-valid";
                }

                if (document.contact_form.password.value == "") {
                    document.contact_form.password.className = "form-control is-invalid";
                    valid = false;
                } else {
                    document.contact_form.password.className = "form-control is-valid";
                }

                if (document.contact_form.confirmPassword.value == "") {
                    document.contact_form.confirmPassword.className = "form-control is-invalid";
                    valid = false;
                } else {
                    document.contact_form.confirmPassword.className = "form-control is-valid";
                }

                if (document.contact_form.secondName.value == "") {
                    document.contact_form.secondName.className = "form-control is-invalid";
                    valid = false;
                } else {
                    document.contact_form.secondName.className = "form-control is-valid";
                }

                if (document.contact_form.firstName.value == "") {
                    document.contact_form.firstName.className = "form-control is-invalid";
                    valid = false;
                } else {
                    document.contact_form.firstName.className = "form-control is-valid";
                }
                let prov = [];
                if (document.contact_form.confirmPassword.value != document.contact_form.password.value) {
                    document.contact_form.password.className = "form-control is-invalid";
                    document.contact_form.confirmPassword.className = "form-control is-invalid";
                    document.getElementById('danger').hidden = false;
                    prov.push(false);
                    valid = false;
                }

                if (document.contact_form.type.value == "") {
                    document.getElementById('type-alert').hidden = false;
                    prov.push(false);
                    valid = false;
                } else {
                    document.getElementById('type-alert').hidden = true;
                }
                
                if (prov.every(elem => elem == true)) {
                    document.getElementById('alert').hidden = true;
                }
                else{
                    document.getElementById('alert').hidden = false;
                }

                return valid;
            }
            </script>
            <form name="contact_form" method="POST" action="/signup" onsubmit="return valuedate();">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Email @if (isset($_COOKIE['error']))
                            @if (($_COOKIE['error'])=="2")
                            <span class="text-danger " id="email-alert">
                                (This email is already in use!)
                            </span>
                            @endif
                            @endif</label>
                        <input type="email" class="form-control" name="email" id="email">
                        <label for="inputAddress">First Name</label>
                        <input type="text" class="form-control" name="firstName" id="firstName">
                        <label for="inputAddress">Second Name</label>
                        <input type="text" class="form-control" name="secondName" id="secondName">

                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Password</label>
                        <input type="password" class="form-control" name="password" id="password">
                        <label for="inputPassword4">Confirm Password</label>
                        <input type="password" class="form-control" id="confirmPassword">
                        <div class="input-group-prepend" style="padding-top: 2rem;">
                            <p><b>What type of user are you?</b><Br>
                                <input id="r1" type="radio" name="type" value="2">
                                <label for="r1">Farmer</label>
                                <input id="r2" type="radio" name="type" value="1">
                                <label for="r2">BeeKeeper</label>
                            </p>
                        </div>
                    </div>
                    <div>
                        <button type="submit" id="btn" class="btn btn-success"
                            style="border-radius: 50px; width: 150px">Sign
                            Up</button>
                        <div class="alert alert-danger" id="alert" hidden="true" style="margin-top: 25px; min-width: 360px;">
                            <ul>
                                <li class="text-danger " hidden="true" id="danger">
                                    Attention: your "Password" and "Confirm Password" do not match, please, try again
                                </li>
                                <li class="text-danger " hidden="true" id="type-alert">
                                    Please, select your type
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div style="height: 200px;">
                    </div>
            </form>
        </div>
    </div>
    <div class="col-md-2 mt-md-0 mt-3">

    </div>
</div>

@endsection