@extends('layouts.app')
@section('title-block')
BeeKeeper
@endsection
@section('content')
<div class="row" style="margin-right: 0px;">
    <div class="col-md-2 mt-md-0 mt-3 side">

    </div>

    <div class="col-md-8 mt-md-0 mt-3 container-main-text " style="">
        <div class="row" style="align-items: center; display: flex;  justify-content:space-between;">
            <div class="column" style=" padding-left: 30px; dysplay: flex;">
                <h1 style="font-size: 1.6em; padding-top: 15px; ">
                    Alerts
                </h1>
                <p> This is a list of your alerts.</p>
            </div>
        </div>
        <div>
            <div class="cont">
                @forelse ($data['alert'] as $spray)
                <div class="col-xs-12 col-sm-6">
                    <div class="blob card " style=" border: solid 1px #ccc; margin-bottom: 15px;">
                        <div class="card-header bg-danger text-white">
                            <h5 >Spray event near: {{$spray->names}}</h5>
                            <h6 >Date: {{$spray->date}}</h6>
                            <h6 >Time: {{$spray->time}}</h6>
                        </div>
                        <div class="card-body bg-warning" style="overflow:hidden;">
                            <div x-data="{ show: false }">
                                <a class="alert-link unselectable" x-on:click="show= ! show"
                                    x-text="show ? 'Insectides (Hide)': 'Insectides (Show)' "> </a>
                                @foreach($spray->list as $list)
                                <hr  x-show.transition.origin.top.right="show"/>
                                <h6 class="card-subtitle mb-2 text-muted unselectable"
                                    x-show.transition.origin.top.right="show">
                                    {{$list}}</h6>
                                @endforeach
                               
                            </div>
                        </div>
                    </div>
                </div>
                @empty
                <div class="text-muted" style="width: 100%; text-align: center">
                    <h5>No Spray Events near your hives</h5>
                </div>
                @endforelse
            </div>
        </div>
        <div class="row" style="align-items: center; display: flex;  justify-content:space-between;">
            <div class="column" style=" padding-left: 30px; ">
                <h1 style="font-size: 1.6em; padding-top: 15px; ">
                    Your Hives
                </h1>
                <p> This is a list of your Spray Events.</p>
            </div>
            <button type="button" class=" btn btn-success add-btn" style="" data-toggle="modal" data-target="#Modal2"
                onClick='location.href="/addHive"'>Add a Hive</button>
        </div>
        <div style="padding-bottom: 30px;">
            <div class="cont">
                @forelse ($data['hives'] as $hive) <div class="col-xs-12 col-sm-6">
                    <div clss="card" style=" border: solid 1px #ccc; margin-bottom: 15px;">
                        <h5 class=" card-header">{{$hive->name}}</h5>
                        <div class="card-body" style="overflow:hidden;">
                            <div x-data="{ show: false }">
                                <a class="alert-link unselectable" x-on:click="show= ! show"
                                    x-text="show ? 'Location (Hide)': 'Location (Show)' "> </a>
                                <p class="card-text" x-show.transition.origin.top.right="show">Coords:
                                    {{$hive->lat}},{{$hive->lng}}</p>
                                <hr />
                            </div>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" @click="deleteTodo(todo.id)" class="btn btn-danger"
                                    style="border-radius: 50px 0 0 50px; width: 100px;"
                                    onClick='location.href="/deleteHive?id={{$hive->id}}"'>Delete</button>
                                <button type="button" style="border-radius: 0 50px 50px 0; width: 100px"
                                    class="btn btn-success"
                                    onClick='location.href="/editHive?id={{$hive->id}}"'>Edit</button>
                            </div>
                        </div>
                    </div>
                </div>
                @empty
                <div class="text-muted" style="width: 100%; text-align: center">
                    <h5>No hives</h5>
                </div>
                @endforelse
            </div>
        </div>
    </div>
    <div class="col-md-2 mt-md-0 mt-3 side">

    </div>
    @endsection