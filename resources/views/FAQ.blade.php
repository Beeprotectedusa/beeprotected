@extends('layouts.app')
@section('title-block')
FAQ's
@endsection
@section('content')
<div class="row " style="margin-right: 0px;">
    <div class="col-md-2 mt-md-0 mt-3 side">
       
    </div>
    <div class="col-md-8 mt-md-0 mt-3 container-main-text">
        <div class="container-fluid">
            <h2>
                Using BeeProtected
            </h2>
            <hr align="center" width="98%" size="2" color="#6c757d" />
            <h3 style="font-size: 1.6em;">
                Will farmers need to notify beekeepers every time they plan to go out and spray a crop?
            </h3>
            <p>
                The system only applies to spraying of insecticides. Users will be able to select or enter either the
                product name or active ingredient drop-down list when they enter an event. You should remember that it
                remains the sprayer operator’s responsibility to read the label of any crop protection product they are
                using and ensure they comply with the label’s requirements. 
            </p>
            <h1 style="font-size: 1.6em;">
                How much advance notification will farmers need to give of a spray event?
            </h1>
            <p style="padding-top: 15px;">
                The Code of Practice for Using Plant Protection Products and most farm assurance schemes generally
                advise farmers give 48 hours’ notice, and farmers should seek to comply with this.
            </p>
            <h1 style="font-size: 1.6em;">
                What happens if a farmer changes their plans?
            </h1>
            <p style="padding-top: 15px;">
                The Code of Practice for Using Plant Protection Products and most farm assurance schemes generally
                advise farmers give 48 hours’ notice, and farmers should seek to comply with this.
            </p>
            <h1 style="font-size: 1.6em;">
                Will beekeepers be informed of a spray event if it has been logged by a farmer before they have mapped
                their hives?
            </h1>
            <p style="padding-top: 15px;">
                Yes. Beekeepers mapping hives for the first time, or mapping new hives, will receive notifications for
                any spray events in the vicinity that farmers have logged which have not already passed. Beekeepers may
                also wish to include the potential locations of their hives, as well as the actual locations so that
                they can make informed decisions if considering moving a hive.
            </p>
            <h1 style="font-size: 1.6em;">
                Who is responsible in contracting scenarios?
            </h1>
            <p style="padding-top: 15px;">
                For the purposes of the system, spray operators who are providing contracting services to farmers or
                landowners should register as “farmers”. You should liaise with the farmer or landowner to avoid any
                duplication in logging events (i.e. both using the system independently for the same spray events).
            </p>
            <h1 style="font-size: 1.6em;">
                Can farmers include more than one field and/or one insecticide in a spray event?
            </h1>
            <p style="padding-top: 15px;">
                Yes. As long as the farmers have added the relevant fields, they can include them all under a single
                spray event (assuming the same crop is being treated with the same insecticide or insecticides). The
                beekeeper will receive a single notification regardless of the number of insecticides being applied.
                However, he will receive a separate notification for each field, depending on whether it is within the
                beekeeper’s defined notification distance.
            </p>
            <h1>
                Security and Confidentiality
            </h1>
            <hr align="center" width="98%" size="2" color="#6c757d" />
            <h1 style="font-size: 1.6em;">
                Will farmers/beekeepers have to identify the location of their farms/where they live?
            </h1>
            <p style="padding-top: 15px;">
                No. Farmers and beekeepers can use a postcode in the map tool to zone in on the approximate location of
                their farm or hives, but they will then simply drop in a pin to mark the location. For farmers, we
                advise doing so in the middle of the field they will be spraying. The location of the field or hives
                will be saved to make the system quicker to use in future. The locations of nearby hives will be visible
                to farmers, but no further information will be available (e.g. the names given to the hives or the
                identification of the user who has mapped them).
            </p>
            <h1 style="font-size: 1.6em;">
                Will beekeepers be told of the exact location of the spray event?
            </h1>
            <p style="padding-top: 15px;">
                No. Beekeepers will be told of the approximate location of the event – for example, “within 3 miles
                northwest of Hive A”. This information will be sufficient to allow beekeepers to take any action they
                deem necessary and will avoid the situation, for instance, where hives are moved closer to the event.
            </p>
            <h1 style="font-size: 1.6em;">
                Will beekeepers be told what product or active ingredient is being used?
            </h1>
            <p style="padding-top: 15px;">
                Beekeepers will be informed of the name of the active ingredient (AI) being applied.
            </p>
            <h1 style="font-size: 1.6em;">
                How will users' personal details be protected?
            </h1>
            <p style="padding-top: 15px;">
                User confidentiality is hugely important to the success of the system, and no information will be
                divulged to other users, without their explicit consent, beyond the basic requirements of the system. It
                should be noted, that in order to register on the system, addresses of users are not required.</p>
            <h1 style="font-size: 1.6em;">
                Even though users' identities are protected, will it be possible for the users to forego protection
                and/or contact each other?
            </h1>
            <p style="padding-top: 15px;">
                Using the E-mail feature, once a beekeeper has been notified of an event they can send an anonymous
                message via the system to the farmer in question. Any subsequent response from farmer to beekeeper will
                also be anonymous. This protects the identities of all users communicating within the system. Should
                users wish to reveal their identities within this correspondence, they are of course free to do so.
                Indeed, as BeeProtected is intended to improve communication between farmers and beekeepers, we hope that
                it will provide the first point of contact between them in building strong and constructive
                relationships.
            </p>
            <h1 style="font-size: 1.6em;">
                How can farmers be confident beekeepers are using the system and not third parties without a genuine
                beekeeping interest?
            </h1>
            <p style="padding-top: 15px;">
                We acknowledge there are concerns that “troublemakers” may seek to use the system for negative reasons.
                However, the simplicity of the system and the specific nature of the information contained in alerts
                means the scope for any malicious activity is extremely limited.
            </p>
            <p style="padding-top: 15px;">
                While no system can guarantee the credential of all its users, BeeProtected reserves the right to remove
                any users we believe are abusing the system, or are not genuine farmers, contractors or beekeepers. This
                includes monitoring the locations of hives and fields to ensure they are in genuine locations.
            </p>
            <p style="padding-top: 15px;">
                It would be hugely disappointing if the activities of an individual or group undermined confidence in
                the system and reduced the number of farmers and beekeepers using it. The only losers in this scenario
                would be the bees the system is trying to protect. We believe this stark fact will deter people from
                using the system for negative purposes.
            </p>
            <h1>
                General Questions
            </h1>
            <hr align="center" width="98%" size="2" color="#6c757d" />
            <h1 style="font-size: 1.6em;">
                Why does the system not allow beekeepers to receive notification of events further than 5miles away?
            </h1>
            <p style="padding-top: 15px;">
                As a rule of thumb, the foraging area around a beehive extends for approximately 2 miles, although bees
                have been observed foraging greater distances from the hive. In order to ensure beekeepers receive
                notifications that are useful to them, and to avoid receiving too many notifications which are not
                relevant, the system restricts the notification area to 5 miles. In reality, it is anticipated that most
                beekeepers will only want to receive notifications closer to home, and the area can be restricted to
                distances down to 1 mile.
            </p>
            <h1 style="font-size: 1.6em;">
                Why are beekeepers not provided with an exact location of where the spray event is taking place?
            </h1>
            <p style="padding-top: 15px;">
                The system seeks to provide a balance between notifying beekeepers with sufficient information to take
                appropriate action to protect their bees, and protecting the personal and operational data of farmer
                users. The approximate information provided by the notification (e.g. “within 3 miles northwest of Hive
                A”) meets this balance.
            </p>
            <h1 style="font-size: 1.6em;">
                Why does the system only include events involving insecticides?
            </h1>
            <p style="padding-top: 15px;">
                The system is designed to assist farmers in complying with best practice and assurance schemes, which
                are designed to help beekeepers protect their bees when a farmer is in the process of applying a
                pesticide to their crop. A number of insecticides have specific label requirements relating to the
                protection of bees, and given that bees are insects, beekeepers are primarily concerned to know when
                farmers are applying an insecticide.
            </p>
            <p style="padding-top: 15px;">
                Including products beyond insecticides runs the risk of it becoming cumbersome, confusing and
                time-consuming for farmers, limiting take-up, without providing further benefit to beekeepers.
                Beekeepers would also be at risk of becoming burdened with irrelevant notifications, undermining their
                confidence in the usefulness of the system.
            </p>
            <h1 style="font-size: 1.6em;">
                Why does the system require farmers to notify what crop they are spraying?
            </h1>
            <p style="padding-top: 15px;">
                The identity of the crop will in some instances be of interest to the beekeeper, depending on the
                attractiveness of certain crops to bees. Farmers can also indicate whether they have a flowering margin
                next to the crop which might be attractive to bees (for instance under an environmental stewardship
                scheme or as part of the Campaign for the Farmed Environment), which would also be of interest to
                beekeepers.
            </p>
            <p style="padding-top: 15px;">
                This feature also acts as a useful aide-memoire to farmers, who need to take account when spraying some
                insecticides of whether the crop is flowering and therefore if there is a risk to bees.
            </p>
            <h1 style="font-size: 1.6em;">
                Does the system include the use of seeds treated with pesticides
            </h1>
            <p style="padding-top: 15px;">
                No. The system is designed to assist farmers in complying with best practice and assurance schemes,
                which currently require notification in the event of spraying.
            </p>
            <h1 style="font-size: 1.6em;">
                Is using the system compulsory for farmers? Why should they use the system at all if they're not
                required to?
            </h1>
            <p style="padding-top: 15px;">
                Best practice and most farm assurance schemes require the notification of beekeepers when spraying, so
                the system will assist in assuring compliance. Just as importantly, the system enables positive and
                clear communication between farmers and beekeepers, helping beekeepers to protect their bees and
                contributing to the excellent work farmers already do in farming in a way that protects the environment
                and biodiversity.
            </p>
            <p style="padding-top: 15px;">
                The system is very easy to use and entering an event should take no more than a minute. Farmers can
                easily do their bit to help protect our pollinators with a minimum of effort.
            </p>
            <h1 style="font-size: 1.6em;">
                Will the website provide further information or services beyond the basic notification system?
            </h1>
            <p style="padding-top: 15px;">
                At present, the website is focused on the spray notification system. However, we hope that in due course
                the website will be able to provide further, relevant information to both farmers and beekeepers to
                encourage best practice in protecting pollinators.
            </p>
        </div>
    </div>
    <div class="col-md-2 mt-md-0 mt-3 side">

    </div>
</div>
@endsection