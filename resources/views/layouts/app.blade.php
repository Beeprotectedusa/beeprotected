<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>@yield('title-block')</title>
    <link rel="icon" type="image/png" href="{{asset('images/icon.png')}}">
    <meta name="viewport" content="width=device-width, initial-scale=0.95">
    <style>
    #map {
        width: 100%;
        height: 400px;
        background-color: grey;
    }

    a.footer {
        color: #343a40;
        text-decoration: underline;
    }

    a.footer:visited {
        color: #6c757d;
    }

    button.see {
        padding-bottom: 10px;
        padding-top: 10px;

    }

    div.center {
        text-align: center;
    }

    h1.outher {}

    a.footer:active {
        color: #6c757d;
    }

    #layer1: {
        z-index: 15;
    }
    </style>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    
    <script src="https://cdn.boomcdn.com/libs/instafeed-js/1.4.1/instafeed.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-175661318-1"></script>
    <script type="text/javascript">
        var feed = new Instafeed({
        accessToken: 'IGQVJWZATlUQ0tPSl9fUm9TOTExR1dUVFVxd1JTR1hCUmZA6dnBCLU1XLXlOS3dzcEZAXYkNVRnhWdE1LcGV3RDNGRERxbUdQNkFyeDlrb3RpX3VIcUk4XzE0b0kzWGZA3NnpIU3RYckR5T0FuUkNwei1yQwZDZD'
        });
        feed.run();
    </script>
    <script>
    window.fbAsyncInit = function() {
        FB.init({
            appId: '755065521937663',
            xfbml: true,
            version: 'v8.0'
        });
        FB.AppEvents.logPageView();
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>

    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-175661318-1');
    </script>


    <script type="text/javascript">
    //IGQVJWdUNFb251eWtDUkM2R2RqQmpFVGwtd213OUxaOVpZASkFHTVpUYV9CZADRWX0tIUW1PUkZABaVFaaXdxUWQ4WUVoT0tHOG1mR212OEN3LWc5TDRNVXBFZAml4S29vVUFublF1NkVrcVRRaGY0YzdPegZDZD
    //https://api.instagram.com/oauth/authorize?client_id=333505221200547&redirect_uri=https://beeprotectedusa.com/&scope=user_profile,user_media&response_type=code 
        $(window).ready(function() {
            let ids = ["Home", "Login", "How it Works?", "Contact us", "FAQ's",
                "A valuable tool connecting beekeepers with farmers & informing of crop protection activities nearby",
                "Farmer", "BeeKeeper", "News"
            ];
            for (let id of ids) {
                let element = document.getElementById(id);
                if (id == document.title) {
                    element.classList.add("active");
                }
            }
        });
    </script>

</head>

<body>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v8.0&appId=755065521937663&autoLogAppEvents=1" nonce="PnsQsMeJ"></script>
    <nav x-data="{ show: true }" class="navbar navbar-expand-lg navbar-light  unselectable"
        style="background-color: #ffcc00;">
        <div class="col" style="min-width: 30%">
            <a class="d-none d-lg-block slideRight"
                style="font-family: inherit; font-weight: 600; font-size: 35px; margin-left: 112px; padding-top: 30px;"
                href="/home">
                <img src="{{asset('images/logo.jpg')}}" width="65" height="65" class="d-inline-block align-top" alt=""
                    loading="lazy"> </a>
            <a class="navbar-brand unselectable d-none d-lg-block"
                style="font-family: inherit; font-weight: 600; font-size: 45px;" href="/home">
                BeeProtected
            </a>
            <a class="navbar-brand unselectable d-lg-none"
                style="font-family: inherit; font-weight: 600; font-size: 30px;" href="/home">
                <img src="{{asset('images/logo.jpg')}}" width="45" height="45" class="d-inline-block align-top" alt=""
                    loading="lazy"> BeeProtected
            </a>
        </div>
        <div class="container-hamburger navbar-toggler" style="border-color: #ffc107;" onclick="myFunction(this)"
            data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false"
            aria-label="Toggle navigation">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
        </div>
        <script>
        function myFunction(x) {
            x.classList.toggle("change");
        }
        </script>
        <div class="collapse navbar-collapse slideDown" id="navbarText" style="padding-left: 15px;">
            <div>
                <div class="strapline d-none  d-lg-block">
                    <p>
                        Connecting beekeepers and farmers
                    </p>
                </div>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item header-lnk"
                        id="A valuable tool connecting beekeepers with farmers & informing of crop protection activities nearby">
                        <a class="nav-link custom" href="/home">Home<span class="sr-only">(current)</span></a>
                    </li>
                    @if (isset($_COOKIE['id']) and isset($_COOKIE['hash']))
                    @if ($_COOKIE['type'] == 2)
                    <li class="nav-item" id="Farmer">
                        <a class="nav-link header-lnk" href="/login">Account<span class="sr-only">(current)</span></a>
                    </li>
                    @else
                    <li class="nav-item" id="BeeKeeper">
                        <a class="nav-link header-lnk" href="/login">Account<span class="sr-only">(current)</span></a>
                    </li>
                    @endif
                    @else
                    <li class="nav-item" id="Login">
                        <a class="nav-link header-lnk" href="/login">Login<span class="sr-only">(current)</span></a>
                    </li>
                    @endif
                    <li class="nav-item" id="How it Works?">
                        <a class="nav-link header-lnk" href="/hiw">How it Works? <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item" id="FAQ's">
                        <a class="nav-link header-lnk" href="/FAQ">FAQ's <span class="sr-only">(current)</span></a>
                    </li>
                    @if (isset($_COOKIE['id']) and isset($_COOKIE['hash']))

                    <li class="nav-item ">
                        <button class="btn btn-outline-dark my-2 my-sm-0" onClick='location.href="/logout"'
                            type="button">Log
                            out</button>
                    </li>

                    @endif
                </ul>
            </div>
        </div>
    </nav>

    <div x-data="{ msg: document.title }" class="container-fluid d-none  d-lg-block unselectable"
        style="background-color: #343432; height: 120px; background-repeat: repeat-x; background-image: url({{asset('images/texture.svg')}})">
        <div class="row" style="Justify-content: center; align-items: center; height: 100%;">
            <div class=" animate">
                <div class="container" style="text-color: white; text-align: center;">
                    <h1 class="text-light" x-text="msg" id="sub">

                    </h1>
                </div>
            </div>
        </div>
    </div>
    <div x-data="msgs()" class=" d-lg-none container-fluid unselectable"
        style="background-color: #343432; height: 60px; background-repeat: repeat-x; background-image: url({{asset('images/texture.svg')}})">
        <div class="row" style="Justify-content: center; align-items: center; height: 100%;">
            <div class="subHeader">
                <div class="container" style="text-color: white; ">
                    <h1 class="text-light" x-text="msg()" id="sub">

                    </h1>
                </div>
            </div>
        </div>
    </div>
    <script>
    function msgs() {
        return {
            msg: function() {
                if (document.title ==
                    "A valuable tool connecting beekeepers with farmers & informing of crop protection activities nearby"
                )
                    return "Home";
                else
                    return document.title;
            },
        };
    }
    </script>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}" />
    @yield('content')
    <footer class="page-footer font-small  pt-4" style="width: 100%; background-color: #ffcc00;">
        <div class="container-fluid text-center text-md-left">
            <div class="row" style="justify-content: space-between; padding-right: 15px; padding-left: 15px;">
                <div class="col-md-9 mt-md-0 mt-3">
                    <p style="font-family: Lora,serif; font-size: 25px;">BeeProtected aims to help the communication
                        between beekeepers and farmers.</p>
                </div>
                <hr class="clearfix w-100 d-md-none pb-3">

                <div class="col-md-3 mb-md-0 mb-3">
                    <ul class="list-unstyled">
                        <li>
                            <a class="footer-lnk" href="/FAQ">FAQs</a>
                        </li>
                        <li>
                            <a class="footer-lnk" href="/terms">Terms</a>
                            |
                            <a class="footer-lnk" href="/privacy">Privacy</a>
                        </li>
                        <li>
                            <a class="footer-lnk" href="#">support@beeprotectedusa.com</a>
                        </li>
                        <div id="msg_pop" style="display: none;">
                            <span class="msg_close unselectable"
                                onclick="document.getElementById('msg_pop').style.display='none'; document.getElementById('msg_pop').className += 'fadeOut'; return false;">X</span>
                            <h4 class="unselectable" style="margin-bottom: 15px;">If you have any queries about Bee
                                Connected please contact us via email.</h4>
                            support@beeprotectedusa.com
                        </div>
                        <li>
                            <a class="footer-lnk"
                                onclick="document.getElementById('msg_pop').style.display='block';document.getElementById('msg_pop').className += 'fadeIn'; return false;"
                                href="#">Contact us</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright text-center py-3">© 2020 Copyright:
            <a class="footer" href="https://mdbootstrap.com/"> MDBootstrap.com</a>
        </div>
    </footer>
</body>

<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

</html>