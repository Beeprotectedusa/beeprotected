@extends('layouts.app')
@section('title-block')
Restore Password
@endsection
@section('content')
<script>
function validate_form() {
    "use strict"
    let valid = true;

    if (document.contact_form.email.value == "") {
        document.contact_form.email.className = "form-control is-invalid";
        valid = false;
    } else {
        document.contact_form.email.className = "form-control is-valid";
    }

    return valid;

}
</script>
<div class="row" style="margin-right: 0px;">
    <div class="col-md-2 mt-md-0 mt-3 side">

    </div>
    <div class="col-md-8 mt-md-0 mt-3 container-main-text" style="padding-top: 15px">
        <div class="container-fluid">
            <div class="container-fluid" style="width:100%; max-width: 600px;  margin-top: 5%; ">
                <h3>Please, enter your E-mail </h3>
                <form name="contact_form" action="/restore" method="POST" onsubmit="return validate_form()" novalidate>
                    @csrf
                    <div class="input-group mb-3">
                        <input name="email" type="text" class="form-control" style="border-radius: 25px;  height: 50px;"
                            placeholder="Your email..." aria-label="Имя пользователя" aria-describedby="basic-addon1">
                    </div>
                    @if (isset($_COOKIE['error']))
                    @if (($_COOKIE['error'])=="1")
                    <p class="text-danger " style="padding-left: 10px;" id="danger">No such E-mail in database</p>
                    @endif
                    @endif

                    <div class="controls">
                        <div class="column">
                    
                        </div>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="submit" style=" width: 100px"
                                class="btn btn-success">Submit</button>
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
        <div class="col-md-2 mt-md-0 mt-3 side">

        </div>
    </div>

    @endsection