@extends('layouts.app')
@section('title-block')
How it Works?
@endsection
@section('content')
<div class="row" style="margin-right: 0px;">
    <div class="col-md-2 mt-md-0 mt-3 side">

    </div>
    <div class="col-md-8 mt-md-0 mt-3 container-main-text" style="padding-top: 15px">
        <div class="container-fluid">
            <h2>
                BeeProtected is a new way of undertaking a long-standing practice: farmers informing beekeepers of
                an
                intention
                to apply an insecticide.
            </h2>
            <hr align="center" width="98%" size="2" color="#6c757d" />
            <p>
                Currently, best agricultural practice, as advocated by the USA code of practice for pesticide use and
                farm assurance schemes, requires that notification takes place when certain crop protection products are
                used.

                BeeProtected is a simple, web-based system that can be used via a desktop or laptop computer, tablet or
                smartphone. Farmers can register on the system and then identify a field they are planning to spray with
                an insecticide by dropping a pin in the on-screen map. Then, using the drop down menus, they simply
                enter the insecticide they will be using, the crop they are spraying, and the date they are spraying,
                and a basic notification will be sent out to neighbouring beekeepers registered on the system.
            </p>
            <button type="button" class="see btn btn-success" data-toggle="modal" data-target="#Modal1">Farmers See How
                It Works</button>
            <p style="padding-top: 15px;">
                Beekeepers can register on the system and simply map the location of their bee hives by dropping a pin
                in the on-screen map. They will then receive a notification by email when a spray event is due to take
                place within the vicinity of a hive. Beekeepers will be able to decide how close a spray event has to be
                to them before they are notified, up to a maximum of 5miles. It is then up to the beekeeper to decide
                what
                action to take, if any.
            </p>
            <button type="button" class="see btn btn-success" data-toggle="modal" data-target="#Modal2">Beekeepers See
                How It Works</button>
            <p style="padding-top: 15px;">
                Want to know more? Take a look at our <a class="footer" href="/FAQ">FAQ's</a>
            </p>
        </div>
    </div>
    <div class="col-md-2 mt-md-0 mt-3 side">

    </div>
</div>
<!-- Modals -->
<div class="modal fade" id="Modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Farmers See How
                    It Works</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="carousel" style="width: 100%;" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active" data-interval="6000">
                            <img class="d-block" style="width: 110%; image-rendering: crisp-edges;"
                                src="{{asset('images/farmer1.jpg')}}" alt="First step">
                        </div>
                        <div class="carousel-item" data-interval="6000">
                            <img class="d-block" style="width: 100%; image-rendering: crisp-edges;"
                                src="{{asset('images/farmer2.jpg')}}" alt="Second step">
                        </div>
                        <div class="carousel-item" data-interval="6000">
                            <img class="d-block" style="width: 100%; image-rendering: crisp-edges;"
                                src="{{asset('images/farmer3.jpg')}}" alt="Third step">
                        </div>
                        <div class="carousel-item" data-interval="6000">
                            <img class="d-block" style="width: 100%; image-rendering: crisp-edges;"
                                src="{{asset('images/farmer4.jpg')}}" alt="Fourth step">
                        </div>
                        <div class="carousel-item" data-interval="6000">
                            <img class="d-block" style="width: 100%; image-rendering: crisp-edges;"
                                src="{{asset('images/farmer5.jpg')}}" alt="Fives step">
                        </div>
                        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="Modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Beekeepers See
                    How It Works</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="carousel1" style="width: 100%;" class="carousel slide" data-ride="carousel1">
                    <div class="carousel-inner">
                        <div class="carousel-item active" data-interval="6000">
                            <img class="d-block" style="width: 110%; image-rendering: crisp-edges;"
                                src="{{asset('images/beekeeper1.jpg')}}" alt="First step">
                        </div>
                        <div class="carousel-item" data-interval="6000">
                            <img class="d-block" style="width: 100%; image-rendering: crisp-edges;"
                                src="{{asset('images/beekeeper2.jpg')}}" alt="Second step">
                        </div>
                        <div class="carousel-item" data-interval="6000">
                            <img class="d-block" style="width: 100%; image-rendering: crisp-edges;"
                                src="{{asset('images/beekeeper3.jpg')}}" alt="Third step">
                        </div>
                        <div class="carousel-item" data-interval="6000">
                            <img class="d-block" style="width: 100%; image-rendering: crisp-edges;"
                                src="{{asset('images/beekeeper4.jpg')}}" alt="Fourth step">
                        </div>
                        <div class="carousel-item" data-interval="6000">
                            <img class="d-block" style="width: 100%; image-rendering: crisp-edges;"
                                src="{{asset('images/beekeeper5.jpg')}}" alt="Fives step">
                        </div>
                        <div class="carousel-item" data-interval="6000">
                            <img class="d-block" style="width: 100%; image-rendering: crisp-edges;"
                                src="{{asset('images/beekeeper6.jpg')}}" alt="Fives step">
                        </div>
                        <a class="carousel-control-prev" href="#carousel1" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel1" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
@endsection