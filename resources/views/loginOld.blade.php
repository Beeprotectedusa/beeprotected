@extends('layouts.app')

@section('title-block')
Login
@endsection

@section('content')
<script>
function validate_form() {
    "use strict"
    let valid = true;

    if (document.contact_form.email.value == "") {
        document.contact_form.email.className = "form-control is-invalid";
        valid = false;
    } else {
        document.contact_form.email.className = "form-control is-valid";
    }
    if (document.contact_form.password.value == "") {
        document.contact_form.password.className = "form-control is-invalid";
        valid = false;
    } else {
        document.contact_form.password.className = "form-control is-valid";
    }

    return valid;
}
</script>
<div class=" card animate " style="border-radius: 25px; z-index: 1000;
    width: 30%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%);
    margin-top: 320px;
    position: absolute;
    min-width: 350px;
    box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
}" >
    <div class="card-body">
        <h2 class="card-title" style="padding-left: 10px;">Login</h5>
            <form name="contact_form" action="/signin" method="POST" onsubmit="return validate_form()" novalidate>
                @csrf
                <div class="input-group mb-3">
                    <input name="email" type="text" class="form-control" style="border-radius: 25px;"
                        placeholder="Your email..." aria-label="Имя пользователя" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-3">
                    <input name="password" type="password" class="form-control" style="border-radius: 25px;"
                        placeholder="Password..." aria-label="Имя получателя" aria-describedby="basic-addon2">
                </div>
                @if (isset($_COOKIE['error']))
                @if (($_COOKIE['error'])=="1")
                <p class="text-danger " style="padding-left: 10px;" id="danger">incorrect Password or Login</p>
                @endif
                @endif
                <div class="controls">
                    <div class="column">
                        <a href="#" class="">Restore password</a>
                    </div>
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" onClick='location.href="/register"' class="btn btn-secondary"
                            style="border-radius: 50px 0 0 50px; width: 100px;">Sign up</button>
                        <button type="submit" style="border-radius: 0 50px 50px 0; width: 100px"
                            class="btn btn-success">Log In</button>
                    </div>
                </div>

            </form>
    </div>
</div>
<div class="page-containter" id="layer">
    <div id="carouselExampleSlidesOnly" style="width: 100%;" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item" data-interval="6000">
                <img class="d-block semi crop" style="width: 100%; height: 710px;" src="{{asset('images/lay2.jpg')}}"
                    alt="Второй слайд">
            </div>
            <div class="carousel-item" data-interval="6000">
                <img class="d-block semi crop" style="width: 100%; height:  710px;" src="{{asset('images/lay3.jpg')}}"
                    alt="Третий слайд">
            </div>
            <div class="carousel-item active" data-interval="6000">
                <img class="d-block semi crop" style="width: 100%; height:  710px;" src="{{asset('images/lay1.jpg')}}"
                    alt="Первый слайд">
            </div>
        </div>
    </div>
</div>
@endsection