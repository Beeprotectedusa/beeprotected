@extends('layouts.app')
@section('title-block')
Add Field
@endsection
@section('content')
<div class="row" style="margin-right: 0px; background-color: #f9f9f9;">
    <div class="col-md-2 mt-md-0 mt-3">

    </div>
    <div class="col-md-8 mt-md-0 mt-3 container-main"
        style="padding-top: 15px; padding-bottom: 60px; background-color: #FFF; border: solid 1px #e2e2e2;">
        <div class="container-fluid">
            <h1>
                Edit Spray Event
            </h1>
            <script>
            $(function() {
                $("#datepicker").datepicker({
                    dateFormat: 'yy-mm-dd'
                });
            });
            $(document).ready(function() {
                $('.timepicker').timepicker({
                    timeFormat: 'h:mm p',
                    interval: 30,
                    defaultTime: "{{$data['spray']->time}}",
                    dynamic: false,
                    scrollbar: true
                });
            });

            function toggle() {
                if (document.getElementById('r1').checked == true) {
                    document.getElementById('Insecticides_prod').hidden = false;
                    document.getElementById('Insecticides_act').hidden = true;
                } else {
                    document.getElementById('Insecticides_prod').hidden = true;
                    document.getElementById('Insecticides_act').hidden = false;
                }
            }

            function valuedate() {
                let valid = true;
                if (document.contact_form.datepicker.value == "") {
                    document.contact_form.datepicker.className = "form-control is-invalid";
                    valid = false;
                } else {
                    document.contact_form.datepicker.className = "form-control is-valid";
                }

                if (document.contact_form.List.value == "") {
                    document.getElementById('alert').hidden = false;
                    document.getElementById('type-alert').hidden = false;
                    valid = false;
                }
                else{
                    document.getElementById('type-alert').hidden = true;
                }
                if (document.contact_form.Fields.value == "") {
                    document.getElementById('alert').hidden = false;
                    document.getElementById('danger').hidden = false;
                    valid = false;
                }
                else{
                    document.getElementById('danger').hidden = true;
                }
                if (document.contact_form.List.value != "" && document.contact_form.Fields.value != ""){
                    document.getElementById('alert').hidden = true;
                }
                return valid;
            }
            </script>
           
            <form name="contact_form" method="POST" action="/editSprayEvent" onsubmit="return valuedate ();" novalidate>
                @csrf
                <input type="text" class="form-control" aria-describedby="basic-addon2" name="id"
                            id="location"  hidden="true" value="{{$_GET['id']}}">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Event Date</label>
                        <input type="text" class="form-control" id="datepicker" name="date" value = '{{$data["spray"]->date}}'></p>
                    </div>
                </div>
                <div class="form-row">
                    <script>
                    function fields() {
                        return {
                            fields: [],
                            toggleField: function(id) {
                                let field = this.fields.find(field => field.id === id);
                                if (field){
                                field.completed = !field.completed;}
                                let res = this.fields.filter( field => field.completed);
                                document.getElementById('Fields').value = res.reduce(function(sum,
                                        current) {
                                        return sum + current.id + '_';
                                    }, "");
                                    document.getElementById('Names').value = res.reduce(function(sum,
                                        current) {
                                        return sum + current.title + '_';
                                    }, "");
                            },
                            fetchFields: function() {
                                @foreach ($data['fields'] as $field)
                                this.fields.push({
                                        id: '{{$field->id}}',
                                        title: '{{$field->name}}',
                                        completed: false
                                    });
                                @endforeach 
                            }
                        }
                    }

                    function todos() {
                        return {
                            todos: [],
                            inputValue: "",
                            toggleTodo: function(id) {
                                let todo = this.todos.find(todo => todo.id === id);
                                if (!todo) return;
                                todo.completed = !todo.completed;
                            },
                            addTodo: function() {
                                if (!this.inputValue) {
                                    return;
                                }
                                if (!this.todos.find(item => item.title == this.inputValue)) {
                                    this.todos.push({
                                        id: Date.now(),
                                        title: this.inputValue,
                                        completed: false
                                    });
                                    document.getElementById('List').value = this.todos.reduce(function(sum,
                                        current) {
                                        return sum + current.title + '_';
                                    }, "");
                                } else {

                                    alert('Already in list');
                                }
                                this.inputValue = "";
                            },
                            deleteTodo: function(id) {
                                this.todos = this.todos.filter(todo => todo.id !== id);
                            },
                            checkTodo: function() {
                                if (!todos.length) {
                                    return true;
                                } else
                                    return false;
                            },
                            fetchTodos: function() {
                                let n=0;
                                @foreach ($data['spray']->list as $list)
                                if ('{{$list}}' != "")
                                this.todos.push({
                                        id: Date.now()+n,                                      
                                        title: '{{$list}}',
                                        completed: false
                                    });
                                    n++;
                                @endforeach 
                                document.getElementById('List').value = this.todos.reduce(function(sum,
                                        current) {
                                        return sum + current.title + '_';
                                    }, "");
                            },
                            addFromSelect: function() {
                                let selectedText;
                                if (document.getElementById('r1').checked == true) {
                                    selectedText = $('#Insecticides_prod option:selected').text();
                                } else {
                                    selectedText = $('#Insecticides_act option:selected').text();
                                }

                                if (!this.todos.find(item => item.title == selectedText)) {

                                    this.todos.push({
                                        id: Date.now(),
                                        title: selectedText,
                                        completed: false
                                    });
                                    document.getElementById('List').value = this.todos.reduce(function(sum,
                                        current) {
                                        return sum + current.title + '_';
                                    }, "");

                                } else {
                                    alert('Already in list');
                                }
                            },
                        };
                    }
                    </script>
                    <div class="form-group col-md-6">
                        <div x-data="todos()" x-init="fetchTodos()" class="app">
                            <div class="row">
                                <div class="input-group-prepend" style="padding-left: 15px;">
                                    <p>Select by: <Br>
                                        <input id="r1" type="radio" name="type" value="2" checked onChange="toggle()">
                                        <label for="r1">Product name</label>
                                        <input id="r2" type="radio" name="type" value="1" onChange="toggle()">
                                        <label for="r2">Active ingredient</label>
                                    </p>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <select class="form-control pesticideProducts thin valid" id="Insecticides_prod"
                                    name="Insecticides">
                                    <option value="3526">A13219F</option>
                                    <option value="3414">Aceta 20 SG</option>
                                    <option value="3500">Actellic 50 EC</option>
                                    <option value="3501">Actellic Smoke Generator No. 20</option>
                                    <option value="3345">Afrisect 500 EC</option>
                                    <option value="3444">Agree 50 WG</option>
                                    <option value="3530">Agrovista Reggae</option>
                                    <option value="3490">Agrovista Trotter</option>
                                    <option value="3449">Akofos 480 EC</option>
                                    <option value="3323">Al-cyper Ec</option>
                                    <option value="3324">Alert</option>
                                    <option value="3325">Alpha C 6 ED</option>
                                    <option value="3318">Amec</option>
                                    <option value="3540">Angri</option>
                                    <option value="3415">Antelope</option>
                                    <option value="3497">Aphox</option>
                                    <option value="3447">Applaud 25 SC</option>
                                    <option value="3448">Applaud 25 WP</option>
                                    <option value="3505">Atilla</option>
                                    <option value="3474">Austral Plus</option>
                                    <option value="3435">Azatin</option>
                                    <option value="3337">Ballad</option>
                                    <option value="3477">Balliol</option>
                                    <option value="3357">Bandu</option>
                                    <option value="3464">Barclay Alphasect</option>
                                    <option value="3406">Batavia</option>
                                    <option value="3438">Biocure</option>
                                    <option value="3531">Biscaya</option>
                                    <option value="3445">Botanigard WP</option>
                                    <option value="3439">Bruco</option>
                                    <option value="3532">Calypso</option>
                                    <option value="3452">Carpovirusine</option>
                                    <option value="3453">Carpovirusine EVO 2</option>
                                    <option value="3326">Ceranock</option>
                                    <option value="3319">Clayton Abba</option>
                                    <option value="3411">Clayton Bonsai</option>
                                    <option value="3465">Clayton Cajole</option>
                                    <option value="3533">Clayton Cayman</option>
                                    <option value="3334">Clayton Courage</option>
                                    <option value="3440">Clayton Expel</option>
                                    <option value="3502">Clayton Galic</option>
                                    <option value="3478">Clayton Lambada</option>
                                    <option value="3375">Clayton Lanark</option>
                                    <option value="3407">Clayton Occupy</option>
                                    <option value="3498">Clayton Pirimicarb</option>
                                    <option value="3370">Clayton Purser</option>
                                    <option value="3466">Clayton Slalom</option>
                                    <option value="3376">Clayton Sparta</option>
                                    <option value="3519">Clayton Spirit</option>
                                    <option value="3416">Clayton Vault</option>
                                    <option value="3467">Clayton Vindicate</option>
                                    <option value="3520">Clayyton Malin</option>
                                    <option value="3377">Cleancrop Argent</option>
                                    <option value="3378">CleanCrop Corsair</option>
                                    <option value="3408">Cleancrop Sierra</option>
                                    <option value="3379">Colt 10 CS</option>
                                    <option value="3514">Conserve</option>
                                    <option value="3327">Contest</option>
                                    <option value="3335">Coragen</option>
                                    <option value="3454">Cyd-X</option>
                                    <option value="3455">Cyd-X Duo</option>
                                    <option value="3456">Cyd-X Xtra</option>
                                    <option value="3346">Cyper 500</option>
                                    <option value="3338">Cyren</option>
                                    <option value="3347">Cythrin 500 EC</option>
                                    <option value="3348">Cythrin Max</option>
                                    <option value="3512">Dairy Fly Spray</option>
                                    <option value="3380">Dalda 5</option>
                                    <option value="3358">Decis</option>
                                    <option value="3359">Decis Forte</option>
                                    <option value="3360">Decis Protech</option>
                                    <option value="3425">Degesch Fumigation Tablets</option>
                                    <option value="3481">Degesch Plate</option>
                                    <option value="3482">Degesch Plates</option>
                                    <option value="3441">Delfin WG</option>
                                    <option value="3361">Deltason-D</option>
                                    <option value="3426">Detia Gas Ex-P</option>
                                    <option value="3427">Detia Gas-Ex-B</option>
                                    <option value="3428">Detia Gas-Ex-T</option>
                                    <option value="3462">Dimilin Flo</option>
                                    <option value="3442">Dipel DF</option>
                                    <option value="3339">Dursban WG</option>
                                    <option value="3320">Dynamec</option>
                                    <option value="3479">Eminentos 10 CS</option>
                                    <option value="3405">Envidor</option>
                                    <option value="3340">Equity</option>
                                    <option value="3399">Eradicoat</option>
                                    <option value="3400">Eradicoat Max</option>
                                    <option value="3328">Eribea</option>
                                    <option value="3521">Evure</option>
                                    <option value="3534">Exemptor</option>
                                    <option value="3515">Exocet</option>
                                    <option value="3371">Explicit</option>
                                    <option value="3329">Fastac</option>
                                    <option value="3330">Fastac ME</option>
                                    <option value="3331">Fasthrin 10EC</option>
                                    <option value="3472">Flipper</option>
                                    <option value="3436">Flocter</option>
                                    <option value="3503">Flycatcher</option>
                                    <option value="3527">Force ST</option>
                                    <option value="3349">Forester</option>
                                    <option value="3541">Fury 10 EW</option>
                                    <option value="3333">Gandalf</option>
                                    <option value="3450">Garrison</option>
                                    <option value="3362">GAT Decline 2.5 EC</option>
                                    <option value="3417">Gazelle</option>
                                    <option value="3418">Gazelle SG</option>
                                    <option value="3468">Gocha</option>
                                    <option value="3363">Grainstore</option>
                                    <option value="3364">Grainstore 25EC</option>
                                    <option value="3458">Grainstore ULV</option>
                                    <option value="3459">Grain-Tect ULV</option>
                                    <option value="3522">Greencrop Malin</option>
                                    <option value="3381">Hallmark with Zeon Technology</option>
                                    <option value="3513">Harpun</option>
                                    <option value="3332">Hi-Cyrus</option>
                                    <option value="3367">Hinode</option>
                                    <option value="3419">Insyst</option>
                                    <option value="3529">Isonet T</option>
                                    <option value="3508">Jaboland</option>
                                    <option value="3509">Jabolim</option>
                                    <option value="3413">Kanemite SC</option>
                                    <option value="3382">Karate 2.5WG</option>
                                    <option value="3506">Karbicure</option>
                                    <option value="3480">Karis 10 CS</option>
                                    <option value="3507">Karma</option>
                                    <option value="3383">Kendo</option>
                                    <option value="3321">Killermite</option>
                                    <option value="3493">Killgerm ULV 1500</option>
                                    <option value="3494">Killgerm ULV 500</option>
                                    <option value="3469">Kingpin</option>
                                    <option value="3523">Klartan</option>
                                    <option value="3460">K-Obiol EC 25</option>
                                    <option value="3461">K-Obiol ULV 6</option>
                                    <option value="3384">Kung Fu</option>
                                    <option value="3385">Kusti</option>
                                    <option value="3386">Laidir 10 CS</option>
                                    <option value="3387">Lambda-C 100</option>
                                    <option value="3388">Lambda-C 50</option>
                                    <option value="3389">Lambdastar</option>
                                    <option value="3390">Lamdex Extra</option>
                                    <option value="3350">Langis 300 ES</option>
                                    <option value="3443">Lepinox Plus</option>
                                    <option value="3486">Lycomax</option>
                                    <option value="3457">Madex Top</option>
                                    <option value="3483">Magtoxin Pellet</option>
                                    <option value="3484">Magtoxin Pellets</option>
                                    <option value="3485">Magtoxin Tablet</option>
                                    <option value="3473">Mainman</option>
                                    <option value="3342">Mainspring</option>
                                    <option value="3401">Majestik</option>
                                    <option value="3391">Markate 50</option>
                                    <option value="3412">Masai</option>
                                    <option value="3524">Mavrik</option>
                                    <option value="3489">Mesurol</option>
                                    <option value="3487">Met52 granular bioinsecticide</option>
                                    <option value="3488">Met52 OD</option>
                                    <option value="3343">Minecto One</option>
                                    <option value="3542">Minuet EW</option>
                                    <option value="3409">Movento</option>
                                    <option value="3510">Nakar</option>
                                    <option value="3446">Naturalis-L</option>
                                    <option value="3475">Nemathorin 10G</option>
                                    <option value="3366">NEU 1170 H</option>
                                    <option value="3392">Ninja 5CS</option>
                                    <option value="3476">Nissorun SC</option>
                                    <option value="3336">Pan Genie</option>
                                    <option value="3504">Pan PMT</option>
                                    <option value="3368">Pekitek</option>
                                    <option value="3351">Permasect 500 EC</option>
                                    <option value="3420">Persist</option>
                                    <option value="3429">Phostoxin</option>
                                    <option value="3430">Phostoxin Bag</option>
                                    <option value="3431">Phostoxin Pellet</option>
                                    <option value="3432">Phostoxin Tablet</option>
                                    <option value="3372">Picard 300 WG</option>
                                    <option value="3535">Pintail</option>
                                    <option value="3499">Pirimate 500</option>
                                    <option value="3421">Pure Ace</option>
                                    <option value="3403">Pyrethrum 5 EC</option>
                                    <option value="3341">Pyrinex 48 EC</option>
                                    <option value="3433">Quickphos Pellets 56% GE</option>
                                    <option value="3463">RAK 3+4</option>
                                    <option value="3536">Rana SC</option>
                                    <option value="3451">Reldan 22</option>
                                    <option value="3528">Requiem Prime</option>
                                    <option value="3525">Revolt</option>
                                    <option value="3373">Rumo</option>
                                    <option value="3491">Runner</option>
                                    <option value="3393">RVG Lambda-cyhalothrin</option>
                                    <option value="3495">SB Plant Invigorator</option>
                                    <option value="3394">Sceptre</option>
                                    <option value="3395">Seal Z</option>
                                    <option value="3518">Sequoia</option>
                                    <option value="3352">Signal 300 ES</option>
                                    <option value="3322">Smitten</option>
                                    <option value="3537">Sonido </option>
                                    <option value="3396">Sparviero</option>
                                    <option value="3516">Spindle</option>
                                    <option value="3404">Spruzit</option>
                                    <option value="3538">Standon Zero Tolerance</option>
                                    <option value="3397">Stealth</option>
                                    <option value="3374">Steward</option>
                                    <option value="3470">Sumi-Alpha</option>
                                    <option value="3353">Supasect 500 EC</option>
                                    <option value="3471">Sven</option>
                                    <option value="3422">Symiprid</option>
                                    <option value="3354">Talisma EC</option>
                                    <option value="3355">Talisma UL</option>
                                    <option value="3434">Talunex</option>
                                    <option value="3511">Tec-bom</option>
                                    <option value="3369">Teppeki</option>
                                    <option value="3402">Terminus</option>
                                    <option value="3410">Tetramat</option>
                                    <option value="3517">Tracer</option>
                                    <option value="3365">Trebon 30 EC</option>
                                    <option value="3356">Vazor Cypermax Plus</option>
                                    <option value="3496">Vazor DE</option>
                                    <option value="3344">Verimark 20 SC</option>
                                    <option value="3437">Votivo</option>
                                    <option value="3423">Vulcan</option>
                                    <option value="3424">Vulcan SP</option>
                                    <option value="3492">Vydate 10G</option>
                                    <option value="3398">Warrior</option>
                                    <option value="3539">Zubarone</option>
                                </select>
                                <select hidden="true" class="form-control pesticideIngredients thin valid"
                                    id="Insecticides_act" name="Insecticides" style="" aria-invalid="false">
                                    <option value="1033">abamectin</option>
                                    <option value="1034">alpha-cypermethrin</option>
                                    <option value="1035">beta-cyfluthrin</option>
                                    <option value="1036">chlorantraniliprole</option>
                                    <option value="1037">chlorpyrifos</option>
                                    <option value="1038">cyantraniliprole</option>
                                    <option value="1039">cypermethrin</option>
                                    <option value="1040">cypermethrin + tetramethrin</option>
                                    <option value="1041">deltamethrin</option>
                                    <option value="1042">etofenprox</option>
                                    <option value="1043">fatty acids</option>
                                    <option value="1044">flonicamid</option>
                                    <option value="1045">indoxacarb</option>
                                    <option value="1046">lambda-cyhalothrin</option>
                                    <option value="1047">maltodextrin</option>
                                    <option value="1048">pyrethrins</option>
                                    <option value="1049">spirodiclofen</option>
                                    <option value="1050">spirotetramat</option>
                                    <option value="1051">tebufenpyrad</option>
                                    <option value="1052">acequinocil</option>
                                    <option value="1053">acetamiprid</option>
                                    <option value="1054">aluminium phosphide</option>
                                    <option value="1055">azadirachtin</option>
                                    <option value="1056">Bacillus firmus I - 1582</option>
                                    <option value="1057">Bacillus thuringiensis</option>
                                    <option value="1058">Bacillus thuringiensis aizawai GC-91</option>
                                    <option value="1059">Beauveria bassiana</option>
                                    <option value="1060">buprofezin</option>
                                    <option value="1061">chlorpyrifos-methyl</option>
                                    <option value="1062">Cydia pomonella GV</option>
                                    <option value="1063">diflubenzuron</option>
                                    <option value="1064">dodecadienol + tetradecenyl acetate + tetradecylacetate
                                    </option>
                                    <option value="1065">esfenvalerate</option>
                                    <option value="1066">fludioxonil + tefluthrin</option>
                                    <option value="1067">fosthiazate</option>
                                    <option value="1068">hexythiazox</option>
                                    <option value="1069">magnesium phosphide</option>
                                    <option value="1070">Metarhizium anisopliae</option>
                                    <option value="1071">methiocarb</option>
                                    <option value="1072">methoxyfenozide</option>
                                    <option value="1073">oxamyl</option>
                                    <option value="1074">phenothrin + tetramethrin</option>
                                    <option value="1075">physical pest control</option>
                                    <option value="1076">pirimicarb</option>
                                    <option value="1077">pirimiphos-methyl</option>
                                    <option value="1078">potassium hydrogen carbonate (commodity substance)</option>
                                    <option value="1079">potassium salts of fatty acids</option>
                                    <option value="1080">pyriproxyfen</option>
                                    <option value="1081">spinosad</option>
                                    <option value="1082">sulfoxaflor</option>
                                    <option value="1083">tau-fluvalinate</option>
                                    <option value="1084">tefluthrin</option>
                                    <option value="1085">terpenoid Blend (QRD 460) </option>
                                    <option value="1086">tetradecadienyl + trienyl acetate</option>
                                    <option value="1087">thiacloprid</option>
                                    <option value="1088">zeta-cypermethrin</option>
                                </select>
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-success" @click="addFromSelect()"> Add</button>
                                </div>
                            </div>
                            <h1>List of Instecticides</h1>
                            <p class="text-muted">If you haven't found your Instecticide in list, please input it here
                            </p>
                            <div class="add-todo">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" x-model="inputValue"
                                        placeholder="Input... " />
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-success" @click="addTodo()">Add</button>
                                    </div>
                                </div>
                            </div>
                            <ul class="list">
                                <template x-for="todo in todos" :key="todo.id">
                                    <li class="list unselectable">
                                        <span x-text="todo.title" class="title"></span>
                                        <span @click="deleteTodo(todo.id)" class="delete-todo">&times;</span>
                                    </li>
                                </template>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <div x-data="fields()" x-init="fetchFields()" class="app">
                            <div class="row">
                            </div>
                            <div class="input-group mb-3">
                            </div>
                            <h1>List of affected fields</h1>
                            <ul class="field">
                            <template x-for="field in fields" :key="field.id">
                                <li class="unselectable"
                                    @click="toggleField(field.id)"
                                    :class="{'completed': field.completed}"
                                >
                                    <span x-text="field.title" class="title"></span>
                                </li>
                                </template>
                            </ul>
                        </div>
                        <label style="padding-top: 15px;">Event Start Time</label>
                        <input type="text" class="timepicker form-control" id="timepicker" name="time"></p>
                    </div>
                </div>
                <input type="text" class="form-control" id="List" hidden="true" name="list" />
                <input type="text" class="form-control" id="Fields" hidden="true"  name="fields" />
                <input type="text" class="form-control" id="Names" hidden="true"  name="names" />
                <button type="submit" class="btn btn-success" style="border-radius: 50px; width: 150px">Submit</button>
                <div class="alert alert-danger" id="alert" hidden="true" style="margin-top: 25px;">
                            <ul>
                                <li class="text-danger " hidden="true" id="danger">
                                    Please, select fields
                                </li>
                                <li class="text-danger " hidden="true" id="type-alert">
                                Please, select Instecticides, that you'll use
                                </li>
                            </ul>
                        </div>
            </form>
        </div>
    </div>
    <div class="col-md-2 mt-md-0 mt-3">

    </div>
</div>
@endsection