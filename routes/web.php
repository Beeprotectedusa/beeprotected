<?php

use Illuminate\Support\Facades\Route;

Route::post('/login', 'LoginController@log_con');
Route::get('/logout', 'LoginController@log_out')->name('log_out');
Route::get('/login', 'LoginController@log_con')->name('login_home');
Route::get('/farmer', 'LoginController@farmerDownload')->name('farmer');
Route::get('/beeKeeper', 'LoginController@beeKeeperDownload')->name('beeKeeper');
Route::get('/home', function () {
    return View::make('home');
})->name('home');
Route::get('/news', function () {
    return View::make('news');
})->name('news');
Route::get('/', 'LoginController@first');
Route::get('/addSprayEvent', 'LoginController@getFields');
Route::get('/deleteField', 'MapController@deleteField');
Route::get('/deleteHive', 'MapController@deleteHive');
Route::get('/deleteSprayEvent', 'MapController@deleteSprayEvent');
Route::get('/editSprayEvent', 'LoginController@editSprayEvent');
Route::post('/editSprayEvent', 'MapController@editSprayEvent');
Route::post('/editFieldGo', 'MapController@editField');
Route::post('/addSprayEvent', 'MapController@addSprayEvent');
Route::get('/editField', 'LoginController@editField');
Route::get('/editHive', 'LoginController@editHive');
Route::post('/editHive', 'MapController@editHive');
Route::get('/register', function () {
    return View::make('register');
});
Route::post('/register', function () {
    return View::make('register');
})->name('register');
Route::post('/signup', 'LoginController@signUp');
Route::post('/addField', 'MapController@addField');
Route::post('/log', 'LoginController@log_con');
Route::post('/emailprove', 'LoginController@email_cheak');
Route::get('/FAQ', function () {
    return View::make('FAQ');
});
Route::get('/hiw', function () {
    return View::make('hiw');
});
Route::get('/addHive', function () {
    return View::make('addHive');
});
Route::post('/restore', 'LoginController@restore');
Route::get('/restore', function () {
    return View::make('restore');
})->name('restore');
Route::post('/addHive', 'MapController@addHive');
Route::get('/add', function () {
    return View::make('addField');
});
Route::get('/terms', function () {
    return View::make('terms');
});
Route::get('/change', 'LoginController@change');
Route::get('/test', 'LoginController@test');
Route::post('/change', 'LoginController@changePass');
Route::post('/signin', 'LoginController@login')->name('login');
Route::get('/mail','LoginController@mailer');
